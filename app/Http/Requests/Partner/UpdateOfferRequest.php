<?php

namespace App\Http\Requests\Partner;

use Illuminate\Foundation\Http\FormRequest;

class UpdateOfferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'terms' => 'required|string',
            'is_active' => 'boolean|nullable',
            'gallery_images' => 'array',
            'title' => 'required|string',
            'image' => 'required',
//            'gallery_urls' => 'array'
        ];
    }
}
