<?php

namespace App\Http\Requests\Partner;

use Illuminate\Foundation\Http\FormRequest;

class CreateOfferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'beginning_at' => 'required|date',
            'ending_at' => 'required|date',
            'terms' => 'required|string',
            'is_active' => 'boolean|nullable',
            'gallery_images' => 'array|nullable',
            'percent' => 'required|numeric|between:0,100',
            'type' => 'required|in:SERVICE,RETAIL',
            'title' => 'required|string',
            'image' => 'required|image|mimes:jpeg,png',
            'category_id' => 'required|exists:categories,id|integer'
        ];
    }
}
