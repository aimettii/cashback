<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Offers\GuestOfferResource;
use App\Http\Resources\Offers\OfferResourceCollection;
use App\Models\Category;
use App\Models\Customer;
use App\Models\Offer;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OfferController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $requestData = $request->all();

        $offers = new OfferResourceCollection(Offer::with(['category:id,title,icon', 'partner'])->active()
            ->when(!empty($requestData['category']),
                function (Builder $query) use ($requestData) {
                    $query->where('category_id', $requestData['category']);
                })
            ->when(!empty($requestData['query']),
                function (Builder $query) use ($requestData) {
                    $query->where(function (Builder $query) use ($requestData) {
                        $query
                            ->where('title', 'like', $requestData['query'] . '%')
                            ->orWhere('subtitle', 'like', $requestData['query'] . '%');
                    });
                })
            ->paginate($requestData['per_page'], ['*'], '', $requestData['current_page']));

        return $this->sendResponse($offers, 'Offers retrieved successfully');
    }

    /**
     * @param string $slug
     * @return mixed
     */
    public function show(string $slug)
    {
        $offer = Offer::findBySlugOrFail($slug);

        $offer->views += 1;

        $offer->save();

        return $this->sendResponse(new GuestOfferResource($offer->refresh()), 'Offer retrieved successfully');
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        // todo-sml: temporarily! for demonstration on the client
//        $categories = Category::whereHas('offers', function (Builder $query) {
//            $query->active();
//        })->get();

        $categories = Category::has('offers')->get();

        return $this->sendResponse($categories, 'Categories retrieved successfully');
    }

    /**
     * @return mixed
     */
    public function getFavorites()
    {
        $user = auth()->user();

        if (!$user || !$user->isCustomer()) {
            return $this->sendResponse([], 'Customer favorite offers retrieved successfully');
        }

        $favoriteOffers = $user->customer->favoriteOffers->pluck('id');

        return $this->sendResponse($favoriteOffers, 'Customer favorite offers retrieved successfully');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function toggleFavorite(int $id)
    {
        $user = auth()->user();

        if (!$user || !$user->isCustomer()) {
            abort(403, 'The user is not a customer!');
        }

        $offer = Offer::findOrFail($id);
        $customer = $user->customer;

        $offer->customerFavorites()->toggle($customer->id);
        $favoriteOffers = $user->customer->favoriteOffers->pluck('id');

        return $this->sendResponse([
            'favorites' => $favoriteOffers,
            'offer' => new GuestOfferResource($offer->fresh())
            ], 'Customer favorite offers retrieved successfully');
    }

    public function addShare($id)
    {
        $offer = Offer::findOrFail($id);

        $offer->shares += 1;

        $offer->save();

        return $this->sendResponse(new GuestOfferResource($offer->refresh()), 'Offer retrieved successfully');
    }
}
