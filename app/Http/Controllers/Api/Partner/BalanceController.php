<?php

namespace App\Http\Controllers\Api\Partner;

use App\Http\Controllers\Controller;
use App\Http\Resources\Partner\BalanceResource;
use Illuminate\Http\Request;

class BalanceController extends Controller
{
    public function index()
    {
        return $this->sendResponse(new BalanceResource($this->user->balance), 'Balance retrieved successfully');
    }
}
