<?php

namespace App\Http\Controllers\Api\Partner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function __invoke(Request $request)
    {
        $data = $request->validate(
            [
                'company_name' => ['nullable', 'string', 'min:3'],
                'merchant_name' => ['nullable'],
                'facebook' => ['nullable', 'string'],
                'instagram' => ['nullable', 'string'],
                'linkedin' => ['nullable', 'string'],
                'address' => ['nullable', 'string'],
                'phone' => ['nullable', 'string'],
                'site' => ['nullable', 'string'],
                'logo' => ['nullable'],
                'description_short' => 'nullable|string',
                'description_large' => 'nullable|string',
                'contact_image' => 'nullable'
            ]);

        $oldImages = [];

        if ($request->logo) {
            $data['logo'] = '/storage/' . $data['logo']->store('uploads', 'public');
            $oldImages[] = auth()->user()->partner->logo ? str_replace('/storage', storage_path('app/public'), auth()->user()->partner->logo) : null;
        } else if ($request->logo === null){
            $oldImages[] = auth()->user()->partner->logo ? str_replace('/storage', storage_path('app/public'), auth()->user()->partner->logo) : null;
            $data['logo'] = null;
        } else {
            unset($data['logo']);
        }

        if ($request->contact_image) {
            $data['contact_image'] = '/storage/' . $data['contact_image']->store('uploads', 'public');
            $oldImages[] = auth()->user()->partner->contact_image ? str_replace('/storage', storage_path('app/public'), auth()->user()->partner->contact_image) : null;
        } else if ($request->contact_image === null){
            $oldImages[] = auth()->user()->partner->contact_image ? str_replace('/storage', storage_path('app/public'), auth()->user()->partner->contact_image) : null;
            $data['contact_image'] = null;
        } else {
            unset($data['contact_image']);
        }

        auth()->user()->partner->update($data);

        // Deleting old images
        foreach ($oldImages as $oldImage) {
            if ($oldImage && file_exists($oldImage)) {
                unlink($oldImage);
            }
        }

        $user = auth()->user();

        $user->load('partner');

        return $this->sendResponse($user, 'Success updated partner profile');
    }
}
