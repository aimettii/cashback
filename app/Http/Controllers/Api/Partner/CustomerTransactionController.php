<?php

namespace App\Http\Controllers\Api\Partner;

use App\Events\CustomerTransactionApprovalRequired;
use App\Events\CustomerTransactionApproved;
use App\Http\Controllers\Controller;
use App\Http\Resources\Partner\CustomerTransactionResource;
use App\Http\Resources\Partner\BalanceResource;
use App\Jobs\ProcessTransaction;
use App\Models\CustomerTransaction;
use App\Models\Partner;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class CustomerTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $transactionsQuery = $this->user->partner->customerTransactions();

        if (filter_var($request->recent, FILTER_VALIDATE_BOOLEAN)) {
            $transactionsQuery
                ->join('partners', 'customer_transactions.partner_id', '=', 'partners.id')
                ->leftJoin('offers', function ($join) {
                    $join->on('partners.id', '=', 'offers.partner_id')
                        ->whereColumn([
                            ['beginning_at', '<', 'customer_transactions.created_at'],
                            ['ending_at', '>', 'customer_transactions.created_at']
                        ]);
                })
                ->select('customer_transactions.*')
                ->orderBy('offers.created_at', 'DESC')
                ->orderBy('customer_transactions.created_at', 'DESC');
        }

        return $this->sendResponse(CustomerTransactionResource::collection($transactionsQuery->paginate($request->per_page === "-1" ? $transactionsQuery->count() : $request->per_page)), 'Transactions retrieved successfully');
    }

    /**
     * Confirm transaction status
     *
     * @param $id
     * @return mixed
     */
    public function confirm($id)
    {
        $transaction = $this->user->partner->customerTransactions()->find($id);

        if (is_null($transaction)) {
            return $this->sendError('Not found transaction', 404);
        }

        if ($transaction->fresh()->status !== CustomerTransaction::STATUS_PENDING) {
            return $this->sendError('Bad transaction status for this request', 422);
        }

        try {
            ProcessTransaction::withChain([
                function () use ($transaction) {
                    $transaction->update([
                        'status' => CustomerTransaction::STATUS_COMPLETED,
                        'released_at' => Carbon::now()
                    ]);
                },
            ])->dispatch($transaction)->allOnConnection('sync');
        } catch (\Exception $e) {
            $errMsg = $e->getMessage();
            return $this->sendError('Error while trying to confirm transaction' . (strlen($errMsg) > 0 ? ': ' . $errMsg : ''), 500);
        }

        return $this->sendResponse([
            'updatedBalance' => new BalanceResource($this->user->balance)
        ],'Transaction confirmed successfully');
    }

    /**
     * Confirm service provision
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function confirmServiceProvision($id, Request $request)
    {
        $transaction = $this->user->partner->customerTransactions()->find($id);

        if (is_null($transaction)) {
            return $this->sendError('Not found transaction', 404);
        }

        if ($transaction->service_completed_at !== null || $transaction->type !== CustomerTransaction::TYPE_SERVICE) {
            return $this->sendError('Bad transaction for this request', 423);
        }

        $request->validate([
            'service_completed_at' => 'required|date'
        ]);

        $transaction->update([
            'service_completed_at' => $request->service_completed_at
        ]);

        if ($transaction->status !== CustomerTransaction::STATUS_COMPLETED && $transaction->released_at === null) {
            $transaction->generateReleasedAt();
        }

        if ($transaction->serviceCompletedAtHasArrived() && !$transaction->approved_by_admin) {
            event(new CustomerTransactionApprovalRequired($transaction));
        }

        return $this->sendResponse(new CustomerTransactionResource($transaction->fresh()), 'Transaction service provision confirmed successfully');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function approve(int $id)
    {
        $transaction = $this->user->partner->customerTransactions()->find($id);

        if (is_null($transaction)) {
            return $this->sendError('Not found transaction', 404);
        }

        if ($transaction->type === CustomerTransaction::TYPE_SERVICE) {
            return $this->sendError(
                'Wrong action! The transaction with type "' . CustomerTransaction::TYPE_SERVICE . '" must be confirm by "Confirm the provision of the service"!',
                423);
        }

        if ($transaction->approved_by_partner) {
            return $this->sendError(
                'Wrong action! The transaction has already been approved by the partner!',
                423);
        }

        $transaction->update(['approved_by_partner' => true]);

        if (!$transaction->approved_by_admin) {
            event(new CustomerTransactionApprovalRequired($transaction));
        }

        if ($transaction->isApprovedByAll()) {
            event(new CustomerTransactionApproved($transaction));
        }

        return $this->sendResponse(new CustomerTransactionResource(
            $transaction->fresh()),
            'Transaction service provision confirmed successfully');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function unApprove(int $id)
    {
        $transaction = $this->user->partner->customerTransactions()->find($id);

        if (is_null($transaction)) {
            return $this->sendError('Not found transaction', 404);
        }

        if (!$transaction->canUnApproved()) {
            return $this->sendError(
                'Wrong action! The transaction cannot be unapproved!',
                423);
        }

        $updateData = $transaction->isService()
            ? ['service_completed_at' => null, 'released_at' => null]
            : ['approved_by_partner' => false];

        $transaction->update($updateData);

        return $this->sendResponse(new CustomerTransactionResource(
            $transaction->fresh()),
            'Transaction service provision confirmed successfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param CustomerTransaction $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(CustomerTransaction $transaction)
    {
        if ($transaction->partner_id !== $this->user->partner->id) {
            return $this->sendError('Transaction not found');
        }

        return $this->sendResponse(new CustomerTransactionResource($transaction), 'Transaction retrieved successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
