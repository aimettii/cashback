<?php

namespace App\Http\Controllers\Api\Partner;

use App\Http\Controllers\Controller;
use App\Http\Requests\Partner\CreateOfferRequest;
use App\Http\Requests\Partner\UpdateOfferRequest;
use App\Http\Resources\Partner\OfferResource;
use App\Models\Offer;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->sendResponse(OfferResource::collection($this->user->partner->offers()->limit(10)->get()), 'Offers retrieved successfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateOfferRequest $request)
    {
        $data = $request->only([
            'beginning_at',
            'ending_at',
            'terms',
            'is_active',
            'percent',
            'type',
            'title',
            'gallery_images',
            'image',
            'category_id',
        ]);

        $data['partner_id'] = $this->user->partner->id;

        if (!empty($data['gallery_images'])) {
            $imagePaths = array_map(function ($image) {
                return '/storage/' . $image->store('uploads', 'public');
            }, $data['gallery_images']);

            $data = Arr::collapse([$data, ['gallery_images' => json_encode($imagePaths, true)]]);
        }

        $imagePath = '/storage/' . $data['image']->store('uploads', 'public');
        $data = Arr::collapse([Arr::except($data, 'image'), ['image_url' => $imagePath]]);
        $offer = Offer::create($data);

        return $this->sendResponse(new OfferResource($offer->fresh()), 'Offer created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $offer = $this->user->partner->offers()->where('id', $id)->first();

        if (is_null($offer)) {
            return $this->sendError('Offer not found');
        }

        return $this->sendResponse(new OfferResource($offer), 'Offer retrieved successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateOfferRequest $request
     * @param int $id
     * @return mixed
     */
    public function update(UpdateOfferRequest $request, int $id)
    {

        $offer = $this->user->partner->offers()->where('id', $id)->first();

        if (is_null($offer)) {
            return $this->sendError('Offer not found');
        }

        $data = $request->only([
            'terms',
            'is_active',
            'title',
            'gallery_images',
            'beginning_at',
            'ending_at',
        ]);

        $baseImageExist = $offer->image_url
            ?
            (
            file_exists(str_replace('/storage', storage_path('app/public'), $offer->image_url))
                ? str_replace('/storage', storage_path('app/public'), $offer->image_url)
                : null
            )
            : null;

        if ($request->image instanceof UploadedFile) {
            $imagePath = '/storage/' . $request['image']->store('uploads', 'public');
            $data = Arr::collapse([$data, ['image_url' => $imagePath]]);
            if ($baseImageExist) {
                unlink($baseImageExist);
            }
        }

        // @todo its bug, string values(old uploaded images) was truncated for standart `$request->gallery_images` property
        $galleryImagesUpdated = collect(!empty($_POST['gallery_images']) ? $_POST['gallery_images'] : []);
        $currentGalleryImages = $offer->gallery_images ? json_decode($offer->gallery_images, true) : [];

        $galleryImagesUpdated = $galleryImagesUpdated->merge(collect($request->gallery_images ?? [])->filter(function($galleryImage) {
            return  $galleryImage instanceof UploadedFile;
        })->all())->all();



        foreach ($currentGalleryImages as $currentGalleryImage) {
            if (!in_array($currentGalleryImage, $galleryImagesUpdated)) {
                $galleryImageFile = str_replace('/storage', storage_path('app/public'), $currentGalleryImage);
                if (file_exists($galleryImageFile)) {
                    unlink($galleryImageFile);
                }
            }
        }

        $imagePaths = array_map(function ($image) {
            if ($image instanceof UploadedFile) {
                return '/storage/' . $image->store('uploads', 'public');
            } else {
                return $image;
            }
        }, $galleryImagesUpdated);

        $data['gallery_images'] = json_encode($imagePaths, true);

        $offer->update($data);

        return $this->sendResponse(new OfferResource($offer->fresh()), 'Offer updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $offer = $this->user->partner->offers()->where('id', $id)->first();

        if (is_null($offer)) {
            return $this->sendError('Offer not found');
        }

        try {
            $offer->delete();
        } catch (\Exception $e) {
            return $this->sendError('Error while deleting offer');
        }

        return $this->sendSuccess('Offer deleted successfully');
    }
}
