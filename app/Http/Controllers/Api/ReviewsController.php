<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Offer;
use Illuminate\Http\Request;

class ReviewsController extends Controller
{
    public function index($id)
    {
        return $this->sendResponse(Offer::findOrFail($id)->offerReviews()->where('accepted', true)->get(), 'Reviews retrieved successfully');
    }
}
