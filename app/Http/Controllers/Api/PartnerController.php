<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Partner;
use Illuminate\Http\Request;

class PartnerController extends Controller
{

    public function index(Request $request)
    {
        $partners = Partner::query();
        ///
        return $partners->get();
    }
}
