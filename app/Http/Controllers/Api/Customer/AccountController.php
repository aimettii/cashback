<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Customer\UpdateAccountRequest;
use App\Http\Resources\Customer\AccountResource;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    /**
     * Get account data
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        return $this->sendResponse(new AccountResource($this->user), 'Account data retrieved successfully');
    }

    /**
     * Update account data
     *
     * @param UpdateAccountRequest $request
     * @return
     */
    public function update(UpdateAccountRequest $request)
    {
        $this->user->update($request->all());

        return $this->sendResponse(new AccountResource($this->user->fresh()), 'Account updated successfully');
    }
}
