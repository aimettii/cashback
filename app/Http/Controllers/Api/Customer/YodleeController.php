<?php

namespace App\Http\Controllers\Api\Customer;

use App\Events\CustomerLinkedProvider;
use App\Facades\Yodlee;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class YodleeController extends Controller
{
    /**
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function token()
    {
        // @todo Check on expired token i think...
        $yodlee = Yodlee::getInstance();

        Validator::make(
            [
                'yodlee_login_name' => $this->user->customer->yodlee_login_name
            ],
            [
                'yodlee_login_name' => ['required'],
            ]
        )->validate();

        $token = $yodlee->generateUserToken($this->user->customer->yodlee_login_name);

        if (!$token) {
            return $this->sendError('Fail while retrieved yodlee token', 500);
        }

        $this->user->customer->update([
            'yodlee_access_token' => $token->accessToken,
            'yodlee_token_issued_at' => $token->issuedAt,
            'yodlee_token_expires_in' => $token->expiresIn,
        ]);

        return $this->sendResponse($token, 'Yodlee token retrieved successfully');
    }

    public function linkProvider(Request $request)
    {
        $this->user->customer->update([
            'yodlee_linked_bank' => $request->bankName,
            'yodlee_linked_provider_account_id' => $request->providerAccountId,
            'yodlee_linked_provider_id' => $request->providerId,
        ]);

        event(new CustomerLinkedProvider($this->user->customer));

        return $this->sendSuccess('Providers linked successfully');
    }

    public function getLinkedBankAccounts()
    {
        // @todo Check on expired token i think...
        $yodlee = Yodlee::getInstance();

        $accounts = $yodlee->getLinkedBankAccounts($this->user->customer->yodlee_login_name);

        if ($accounts === false) {
            return $this->sendError('Fail while retrieved linked bank accounts', 500);
        }

        return $this->sendResponse($accounts, 'Linked bank accounts retrieved successfully');
    }

    public function deleteLinkedBankAccounts($accountId)
    {
        // @todo Check on expired token i think...
        $yodlee = Yodlee::getInstance();

        $deleted = $yodlee->deleteLinkedBankAccount($this->user->customer->yodlee_login_name, $accountId);

        if ($deleted === false) {
            return $this->sendError('Fail while deleting linked bank account', 500);
        }

        return $this->sendResponse($deleted, 'Linked bank account deleted successfully');
    }
}
