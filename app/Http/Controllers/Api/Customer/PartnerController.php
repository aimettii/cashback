<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Controller;
use App\Http\Resources\Customer\PartnerResource;
use App\Models\Partner;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    public function index(Request $request)
    {
        $partners = Partner::query();

        $partners = $partners->paginate($request->per_page == '-1' ? $partners->count() : $request->per_page);

        return $this->sendResponse(PartnerResource::collection($partners),'Offers retrieved successfully');
    }
}
