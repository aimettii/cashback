<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Controller;
use App\Http\Resources\CustomerTransactionResource;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Carbon;

class CustomerTransactionController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            if ($this->user && $this->user->customer) {
                return $next($request);
            } else {
                throw new \Exception('User must have "customer" relation');
            }
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $customer = $this->user->customer;

        $paginatedItems = $customer->customerTransactions()->with('partner')
            ->orderByRaw(\DB::raw('DATE(created_at) desc'))
            ->paginate($request->per_page == '-1' ? $customer->customerTransactions()->count() : $request->per_page);

        $paginatedItems = CustomerTransactionResource::collection($paginatedItems)->toResponse(new Request())->getData();

        $transactionsArr = collect(
            $paginatedItems->data
        )
            ->groupBy(function ($item, $key) {
                $day = Carbon::createFromTimeString($item->created_at)->format('Y_m_d');
                return $day;
            })->toArray();


        $transactions = new LengthAwarePaginator($transactionsArr, $paginatedItems->meta->total, $paginatedItems->meta->per_page, $paginatedItems->meta->current_page);

        return $this->sendResponse($transactions, 'Transactions retrieved successfully');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
