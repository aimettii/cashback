<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Controller;
use App\Http\Resources\Customer\BalanceResource;
use Illuminate\Http\Request;

class BalanceController extends Controller
{
    public function index()
    {
        return $this->sendResponse(new BalanceResource($this->user->balance), 'Balance retrieved successfully');
    }
}
