<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Controller;
use App\Models\Review;
use Illuminate\Http\Request;

class ReviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->sendResponse($this->user->customer->offerReviews, 'Reviews retrieved successfully');
    }

    public function listOffersForReview()
    {
        $offers = collect($this->user->customer->related_offers)->map(function ($offer) {
            $review = $offer->offerReviews()->where('customer_id', $this->user->customer->id)->first();
            $offer->reviewed = !!$review;
            $offer->review = $review;
            return $offer;
        });

        return $this->sendResponse($offers, 'Related offers retrieved successfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $id
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, Request $request)
    {
        $offer = collect($this->user->customer->related_offers)->where('id', $id)->first();

        if (! $offer) {
            return $this->sendError('Not found related offer', 404);
        }

        $data = $request->validate([
            'title' => 'required|string',
            'description' => 'required|string',
            'rating' => 'required|integer|between:1,5',
        ]);

        $data['offer_id'] = $id;

        $alreadyReviewed = $this->user->customer->offerReviews()->where('offer_id', $id)->first();

        if ($alreadyReviewed) {
            return $this->sendError('Already reviewed', 423);
        }

        $review = $this->user->customer->offerReviews()->create(
            $data
        );

        return $this->sendResponse($review, 'Reviews created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $review = $this->user->customer->offerReviews()->findOrFail($id);

        return $this->sendResponse($review, 'Reviews created successfully');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
