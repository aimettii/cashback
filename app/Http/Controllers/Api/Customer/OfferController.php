<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Controller;
use App\Http\Resources\Customer\OfferResource;
use App\Models\Offer;
use Illuminate\Http\Request;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $offersQuery = Offer::active();

        if (filter_var($request->favorite, FILTER_VALIDATE_BOOLEAN)) {
            $offersQuery->favoriteForUser($this->user);
        }

        if (filter_var($request->new, FILTER_VALIDATE_BOOLEAN)) {
            $offersQuery->orderBy('created_at', 'DESC');
        }

        if ($request->limit) {
            $offersQuery->limit($request->limit > 100 ? 100 : $request->limit);
        } else {
            $offersQuery->limit(100);
        }

        return $this->sendResponse(OfferResource::collection($offersQuery->get()),'Offers retrieved successfully');
    }

    public function togglefavoriteOffer($offer_id)
    {
        $offer = Offer::active()->where('id', $offer_id)->first();

        if (is_null($offer)) {
            return $this->sendError('Offer not found');
        }

        $this->user->customer->favoriteOffers()->toggle([$offer_id]);

        return $this->sendSuccess('Toggle favorite offer was successfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
