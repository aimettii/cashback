<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CustomerTransaction;
use App\User;
use Illuminate\Http\Request;

class CustomerTransactionController extends Controller
{
    public function index(Request $request)
    {

        $customerTransactions = CustomerTransaction::query();

        if (auth()->user()->isCustomer()) {
            //
        }
        if (auth()->user()->isPartner()) {
            //
        }
        $customerTransactions = $customerTransactions->get();

        return $customerTransactions;
    }
}
