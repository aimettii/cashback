<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\CustomerResource;
use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index(Request $request)
    {
        $customers = Customer::query();

        $customers = $customers->paginate($request->per_page == '-1' ? $customers->count() : $request->per_page);

        return CustomerResource::collection($customers);
    }

    public function store(Request $request)
    {
        $customer = Customer::create($request->all());

        return new CustomerResource($customer);
    }

    public function show(Request $request, $id)
    {
        $customer = Customer::find($id);

        if (is_null($customer)) {
            return 'Not found customer';
        }

        return new CustomerResource($customer);
    }

    public function update(Request $request, $id)
    {
        $customer = Customer::find($id);

        if (is_null($customer)) {
            return 'Not found customer';
        }

        $customer->update($request->all());

        return new CustomerResource($customer);
    }

    public function destroy(Request $request, $id)
    {
        $customer = Customer::find($id);

        if (is_null($customer)) {
            return 'Not found customer';
        }

        try {
            $customer->delete();
        } catch (\Exception $e) {
            return 'Error while deleting customer';
        }

        return 'Customer deleted successfully';
    }
}
