<?php

namespace App\Http\Controllers\Api\Admin;

use App\Events\CustomerTransactionApprovalRequired;
use App\Events\CustomerTransactionApproved;
use App\Http\Controllers\Controller;
use App\Http\Resources\CustomerTransactionResource;
use App\Models\CustomerTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class CustomerTransactionController extends Controller
{
    public function index(Request $request)
    {
        $transactions = CustomerTransaction::query();

        $transactions = $transactions->paginate($request->per_page == '-1' ? $transactions->count() : $request->per_page);

        return CustomerTransactionResource::collection($transactions);
    }

    public function store(Request $request)
    {
        $transaction = CustomerTransaction::create($request->all());

        return new CustomerTransactionResource($transaction);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function show(int $id)
    {
        $transaction = CustomerTransaction::findOrFail($id);

        return $this->sendResponse(new CustomerTransactionResource($transaction), 'Transaction retrieved successfully');
    }

    public function update(Request $request, $id)
    {
        $transaction = CustomerTransaction::find($id);

        if (is_null($transaction)) {
            return 'Not found transaction';
        }

        $transaction->update($request->all());

        return new CustomerTransactionResource($transaction);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function approve(int $id)
    {
        $transaction = CustomerTransaction::findOrFail($id);

        if ($transaction->approved_by_admin) {
            return $this->sendError(
                'Wrong action! The transaction has already been approved by the admin!',
                423);
        }

        $transaction->update(['approved_by_admin' => true]);

        if (!$transaction->approvedByPartner()) {
            event(new CustomerTransactionApprovalRequired($transaction));
        }

        if ($transaction->isApprovedByAll()) {
            event(new CustomerTransactionApproved($transaction));
        }

        return $this->sendResponse(new CustomerTransactionResource(
            $transaction->fresh()),
            'Transaction service provision confirmed successfully');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function unApprove(int $id)
    {
        $transaction = CustomerTransaction::findOrFail($id);

        if ($transaction->isApprovedByAll()) {
            return $this->sendError(
                'Wrong action! The transaction cannot be unapproved!',
                423);
        }

        $transaction->update(['approved_by_admin' => false]);

        return $this->sendResponse(new CustomerTransactionResource(
            $transaction->fresh()),
            'Transaction service provision confirmed successfully');
    }

    public function destroy(Request $request, $id)
    {
        $transaction = CustomerTransaction::find($id);

        if (is_null($transaction)) {
            return 'Not found transaction';
        }

        try {
            $transaction->delete();
        } catch (\Exception $e) {
            return 'Error while deleting transaction';
        }

        return 'Transaction deleted successfully';
    }
}
