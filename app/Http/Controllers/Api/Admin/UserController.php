<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::query();

        if ($request->has('type')) {
            $users = $users->where('type', $request->get('type'));
        }
        /// other filters
        $users = $users->get();

        return $users;
    }

    public function show(User $user)
    {
        if ($user->isCustomer()) {
            $user->load('customer');
        }
        if ($user->isPartner()) {
            $user->load('partner');
        }
        return $user;
    }

    // update partner and customer through this controller
}
