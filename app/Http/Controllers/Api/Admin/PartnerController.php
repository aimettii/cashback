<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\PartnerResource;
use App\Models\Partner;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    public function index(Request $request)
    {
        $partners = Partner::query();

        $partners = $partners->paginate($request->per_page == '-1' ? $partners->count() : $request->per_page);

        return PartnerResource::collection($partners);
    }

    public function store(Request $request)
    {
        $partner = Partner::create($request->all());

        return new PartnerResource($partner);
    }

    public function show(Request $request, $id)
    {
        $partner = Partner::find($id);

        if (is_null($partner)) {
            return 'Not found partner';
        }

        return new PartnerResource($partner);
    }

    public function update(Request $request, $id)
    {
        $partner = Partner::find($id);

        if (is_null($partner)) {
            return 'Not found partner';
        }

        $partner->update($request->all());

        return new PartnerResource($partner);
    }

    public function destroy(Request $request, $id)
    {
        $partner = Partner::find($id);

        if (is_null($partner)) {
            return 'Not found partner';
        }

        try {
            $partner->delete();
        } catch (\Exception $e) {
            return 'Error while deleting partner';
        }

        return 'Partner deleted successfully';
    }

    public function updateBalance(Request $request, $id)
    {
        $request->validate([
            'value' => 'required|regex:/^\d+(\.\d{1,2})?$/'
        ]);
        $partner = Partner::find($id);

        if (is_null($partner)) {
            return 'Not found partner';
        }

        $balance = $partner->user->balance;

        $balance->value = $request->value;

        $balance->save();

        return $this->sendSuccess('Success updated balance of the partner');
    }
}
