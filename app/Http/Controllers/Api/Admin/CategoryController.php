<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Category\CreateRequest;
use App\Http\Requests\Admin\Category\UpdateRequest;
use App\Models\Category;
use App\Services\Admin\Category\CategoryService;

class CategoryController extends Controller
{
    /**
     * @var CategoryService $serice
     */
    private $service;

    /**
     * CategoryController constructor.
     * @param CategoryService $service
     */
    public function __construct(CategoryService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->sendResponse(
            Category::all(),
            'Categories retrieved successfully');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function show(int $id)
    {
        return $this->sendResponse(
            $this->service->show($id),
            'Category retrieved successfully');
    }

    /**
     * @param CreateRequest $request
     * @return mixed
     */
    public function store(CreateRequest $request)
    {
        return $this->sendResponse(
            $this->service->store($request->all()),
            'Category retrieved successfully');
    }

    /**
     * @param UpdateRequest $request
     * @param int $id
     * @return mixed
     */
    public function update(UpdateRequest $request, int $id)
    {
        return $this->sendResponse(
            $this->service->update($id, $request->all()),
            'Category retrieved successfully');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function destroy(int $id)
    {
        return $this->sendResponse(
            $this->service->destroy($id),
            'Category retrieved successfully');
    }
}
