<?php

namespace App\Http\Controllers;

use App\Events\UpdateUserDataEventNotificationFromYodlee;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class YodleeController extends Controller
{
    public function webhook(Request $request)
    {
        Log::info('Yodlee webhook request: ' . json_encode($request->all()));
        $request->validate([
            'event' => 'required|array',
            'event.info' => 'required|string|in:DATA_UPDATES.USER_DATA',
            'event.data' => 'required|array',
            'event.data.userCount' => 'required|integer',
            'event.data.fromDate' => 'required|date',
            'event.data.toDate' => 'required|date',
            'event.data.userData' => 'required|array',
            'event.data.userData.*' => 'required|array',
            'event.data.userData.*.user.loginName' => 'required|string',
            'event.data.userData.*.links' => 'required|array',
        ]);

        Log::info('Handling request');

        dispatch(function () use ($request){
            $userCountUpdated = $request->event['data']['userCount'];
            $fromDate = $request->event['data']['fromDate'];
            $toDate = $request->event['data']['toDate'];
            $usersData = $request->event['data']['userData'];

            Log::info('Updated user count: ' . $userCountUpdated);
            Log::info('FromDate: ' . $fromDate);
            Log::info('toDate: ' . $toDate);

            foreach ($usersData as $userData) {
                $loginName = $userData['user']['loginName'];
                Log::info('LoginName: ' . $loginName);

                $relatedCustomer = Customer::where('yodlee_login_name', $loginName)->first();

                if (is_null($relatedCustomer)) {
                    Log::error('Not found customer by this LoginName: ' . $loginName);
                    continue;
                }

                $linksRelated = collect($userData['links']);

                $link = $linksRelated->firstWhere('rel', 'getUserData');

                if (!is_null($link)) {
                    Log::info('dispatch event...');
                    event(new UpdateUserDataEventNotificationFromYodlee($relatedCustomer, $loginName, $fromDate, $toDate));
                }
            }
        })->afterResponse();

        return $this->sendSuccess('Hello');
    }
}
