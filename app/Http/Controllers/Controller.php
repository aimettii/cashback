<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller as BaseController;
use Response;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();
            return $next($request);
        });
    }

    public function sendResponse($result, $message)
    {
        $arrResponse = self::makeResponse($message, $result);

        if ($result instanceof ResourceCollection && $result->resource instanceof LengthAwarePaginator) {
            $arrResponse = array_merge($arrResponse, (array) $result->toResponse(new Request())->getData());
        }

        if ($result instanceof LengthAwarePaginator) {
            $paginatorArr = $result->toArray();
            $arrResponse['data'] = $paginatorArr['data'];
            unset($paginatorArr['data']);
            $arrResponse = array_merge($arrResponse, ['meta' => $paginatorArr]);
        }

        return Response::json($arrResponse);
    }

    public function sendError($error, $code = 404, int $typeCode = null, string $description = null)
    {
        $arrResponse = self::makeError($error);

        if ($typeCode) {
            $arrResponse['code'] = $typeCode;
        }

        if ($description) {
            $arrResponse['description'] = $description;
        }

        return Response::json($arrResponse, $code);
    }

    public function sendSuccess($message, $code = 200)
    {
        return Response::json([
            'success' => true,
            'message' => $message
        ], $code);
    }

    /**
     * @param string $message
     * @param array  $data
     *
     * @return array
     */
    public static function makeError($message, array $data = [])
    {
        $res = [
            'success' => false,
            'message' => $message,
        ];

        if (!empty($data)) {
            $res['data'] = $data;
        }

        return $res;
    }

    /**
     * @param string $message
     * @param mixed  $data
     *
     * @return array
     */
    public static function makeResponse($message, $data)
    {
        return [
            'success' => true,
            'data'    => $data,
            'message' => $message,
        ];
    }
}
