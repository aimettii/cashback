<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SpaController extends Controller
{
    public function serve()
    {
        $authUser = auth()->user();
        if (! $authUser) {
            abort(403);
        }
        if ($authUser->isAdmin()) {
            $type = 'admin';
        }
        if ($authUser->isCustomer()) {
            $authUser->load('customer');
            $type = 'customer';
        }
        if ($authUser->isPartner()) {
            $authUser->load('partner');
            $type = 'partner';
        }

        return view('dashboard')->with(compact('type', 'authUser'));
    }
}
