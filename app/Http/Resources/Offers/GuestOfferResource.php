<?php

namespace App\Http\Resources\Offers;

use Illuminate\Http\Resources\Json\JsonResource;

class GuestOfferResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'subtitle' => $this->subtitle,
            'category' => $this->category,
            'partner' => $this->partner,
            'image_url' => $this->image_url,
            'gallery' => $this->gallery,
            'terms' => $this->terms,
            'percent' => $this->percent,
            'slug' => $this->slug,
            'favoritesCount' => $this->customerFavorites->count(),
            'views' => $this->views,
        ];
    }
}
