<?php

namespace App\Http\Resources\Customer;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class OfferResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $offerIsFavorite = Auth::user()
            ->customer
            ->favoriteOffers()
            ->where('offer_id', $this->id)
            ->first() ? true : false;

        return parent::toArray($request) + [
                'is_favorite' => $offerIsFavorite
            ];
    }
}
