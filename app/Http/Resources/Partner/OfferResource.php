<?php

namespace App\Http\Resources\Partner;

use Illuminate\Http\Resources\Json\JsonResource;

class OfferResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
         return [
            'id' => $this->id,
            'title' => $this->title,
            'subtitle' => $this->subtitle,
            'category' => $this->category,
            'image_url' => $this->image_url,
            'gallery_urls' => $this->gallery,
            'is_active' => $this->is_active,
            'terms' => $this->terms,
            'beginning_at' => $this->beginning_at,
            'ending_at' => $this->ending_at,
            'percent' => $this->percent,
        ];
    }
}
