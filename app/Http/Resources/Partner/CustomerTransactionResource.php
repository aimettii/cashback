<?php

namespace App\Http\Resources\Partner;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerTransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request) + [
            'customer' => new CustomerResource($this->customer),
//                'partner' => $this->partner,
            'offer' => $this->partner->getActiveOffer($this->created_at),
                'can_un_approved' => $this->can_un_approved,
                'approved_by_partner_with_type' => $this->approved_by_partner_with_type
        ];
    }
}
