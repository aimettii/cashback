<?php

namespace App\Models;

use App\Models\Relations\BelongsToUser;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Balance extends BaseModel
{
    use BelongsToUser;

    public function add($value)
    {
        $this->value += $value;
        $this->save();
    }

    public function subtract($value)
    {
        $this->value -= $value;
        $this->save();
    }

    /**
     * Register balance relation for new created user
     *
     * @param User $user
     * @return mixed
     * @throws \Exception
     */
    public static function register(User $user)
    {
        if ($user->balance) {
            throw new \Exception('Balance relation already created for this user');
        }

        return self::create(['user_id' => $user->id, 'value' => self::registeredDefaultBalance($user)]);
    }

    /**
     * Get default balance value for new registered user
     *
     * @param User $user
     * @return int
     */
    public static function registeredDefaultBalance(User $user) {
        // @todo conditions
        return 0;
    }
}
