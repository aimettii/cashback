<?php

namespace App\Models;

use App\Models\Relations\BelongsToCategory;
use App\Models\Relations\BelongsToManyCustomerFavorite;
use App\Models\Relations\BelongsToPartner;
use App\Models\Relations\HasManyOfferReviews;
use App\User;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;

class Offer extends BaseModel
{
    use BelongsToPartner,
        BelongsToCategory,
        HasManyOfferReviews,
        Sluggable,
        SluggableScopeHelpers,
        BelongsToManyCustomerFavorite;

    const TYPE_SERVICE = 'SERVICE';
    const TYPE_RETAIL = 'RETAIL';

    protected $table = 'offers';

    protected $casts = [
      'percent' => 'float'
    ];

    protected $appends = ['partner_name', 'favorites_count'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope('without_partner', function ($builder) {
            $builder->whereHas('partner');
        });
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['partner.company_name', 'title']
            ]
        ];
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', 1)->where('beginning_at', '<', Carbon::now())->where('ending_at', '>', Carbon::now());
    }

    /**
     * @param $query
     * @param User $user
     * @return Builder
     * @throws \Exception
     */
    public function scopeFavoriteForUser($query, User $user)
    {
        if (!$user->customer) {
            throw new \Exception('This user doesnt have customer relation');
        }

        $favoriteOffersIds = $user->customer->favoriteOffers->pluck('id');

        return $query->whereIn('id', $favoriteOffersIds);
    }

    public function getPartnerNameAttribute()
    {
        return $this->partner->company_name;
    }

    /**
     * @return mixed
     */
    public function getGalleryAttribute(): array
    {
        return json_decode($this->gallery_images, true) ?? [];
    }

    public function getFavoritesCountAttribute()
    {
        return $this->customerFavorites()->count();
    }

    public function getSharesCountAttribute()
    {
        return $this->customerFavorites()->count();
    }
}
