<?php

namespace App\Models;

use App\Models\Relations\BelongsToUser;
use App\Models\Relations\HasManyCustomerTransactions;
use App\Models\Relations\HasManyOfferReviews;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Customer extends BaseModel
{
    use BelongsToUser, HasManyCustomerTransactions, HasManyOfferReviews;  // Relations

    public function favoriteOffers()
    {
        return $this->belongsToMany(Offer::class, 'customer_favorite_offer', 'customer_id', 'offer_id');
    }

    public static function generateYodleeLoginName()
    {
        if (App::environment('local')) {
            $loginName = 'sbMem' . uniqid();
        } else {
            $loginName = 'prodMem' . uniqid();
        }

        return $loginName;
    }

    public function getRelatedOffersAttribute()
    {
        $offers = [];

        foreach ($this->customerTransactions as $customerTransaction) {
            $activeOffer = $customerTransaction->partner->getActiveOffer($customerTransaction->created_at);

            if ($activeOffer) {
                $offers[] = $activeOffer;
            }
        }

        $offers = collect($offers)->unique(function ($offer) {
            return $offer->id;
        })->values()->all();

        return $offers;
    }
}
