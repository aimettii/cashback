<?php

namespace App\Models;

use App\Models\Relations\BelongsToCustomer;
use App\Models\Relations\BelongsToOffer;
use Illuminate\Database\Eloquent\Model;

class Review extends BaseModel
{
    use BelongsToCustomer, BelongsToOffer;

    protected $table = 'offer_reviews';

    protected $casts = [
        'rating' => 'integer',
        'accepted' => 'boolean'
    ];

    protected $with = ['offer', 'customer'];
}
