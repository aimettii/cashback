<?php

namespace App\Models;

use App\Models\Relations\BelongsToUser;
use App\Models\Relations\HasManyCustomerTransactions;
use App\Models\Relations\HasManyOffers;
use Illuminate\Support\Carbon;

class Partner extends BaseModel
{
    use BelongsToUser, HasManyCustomerTransactions, HasManyOffers; // Relations

    protected $table = 'partners';

    public function getActiveOffer(Carbon $date)
    {
        return $this->offers()->where('beginning_at', '<', $date)->where('ending_at', '>', $date)->first();
    }

    public function getRelatedReviewsAttribute()
    {
        $reviews = [];

        foreach ($this->offers as $offer) {
            foreach ($offer->offerReviews->all() as $review) {
                $reviews[] = $review;
            }
        }

        $reviews = collect($reviews)->unique(function ($review) {
            return $review->id;
        })->values()->all();

        return $reviews;
    }
}
