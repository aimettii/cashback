<?php

namespace App\Models;

use App\Models\Relations\BelongsToCustomer;
use App\Models\Relations\BelongsToPartner;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class CustomerTransaction extends BaseModel
{
    use BelongsToCustomer, BelongsToPartner;  // Relations

    const STATUS_PENDING = 'pending';
    const STATUS_COMPLETED = 'completed';

    const TYPE_SERVICE = 'SERVICE';
    const TYPE_RETAIL = 'RETAIL';

    public $table = 'customer_transactions';

    public $casts = [
        'customer_id' => 'integer',
        'partner_id' => 'integer',
    ];

    public $hidden = ['data'];

    /**
     * Generate released_at field by brand return policy
     *
     * @return \Carbon\Carbon|mixed
     * @throws \Exception
     */
    public function generateReleasedAt()
    {
        if (!is_null($this->released_at)) {
            throw new \Exception('Released at already generated!');
        }
        $this->released_at = Carbon::now()->addMonth(); //@todo harcode
        $this->save();
        return $this->released_at;
    }

    public function scopePending($query)
    {
        return $query->where('status', self::STATUS_PENDING);
    }

    public function scopeWithActiveOffer($query)
    {
        return $query->whereHas('partner.offers', function ($query) {
            $query->whereColumn([
                ['beginning_at', '<', 'customer_transactions.created_at'],
                ['ending_at', '>', 'customer_transactions.created_at']
            ]);
        });
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeUnApprovedByAdminWithTypeService(Builder $query)
    {
        return $query
            ->where('type', self::TYPE_SERVICE)
            ->whereDate('service_completed_at', '<', Carbon::now())
            ->where('approved_by_admin', false);
    }

    /**
     * @return bool
     */
    public function isService(): bool
    {
        return $this->type === self::TYPE_SERVICE;
    }

    /**
     * @return bool
     */
    public function isRetail(): bool
    {
        return $this->type === self::TYPE_RETAIL;
    }

    /**
     * @return bool
     */
    public function getCanUnApprovedAttribute(): bool
    {
        return $this->canUnApproved();
    }

    /**
     * @return bool
     */
    public function canUnApproved(): bool
    {
        return $this->status === self::STATUS_PENDING && !$this->isApprovedByAll();
    }

    /**
     * @return bool
     */
    public function getApprovedByPartnerWithTypeAttribute(): bool
    {
        return $this->approvedByPartner();
    }

    /**
     * @return bool
     */
    public function approvedByPartner(): bool
    {
        return $this->type === self::TYPE_SERVICE
            ? (bool) $this->service_completed_at
            : $this->approved_by_partner;
    }

    /**
     * @return bool
     */
    public function serviceCompletedAtHasArrived(): bool
    {
        return $this->service_completed_at && Carbon::parse($this->service_completed_at)->lt(Carbon::now());
    }

    /**
     * @return bool
     */
    public function isApprovedByAll(): bool
    {
        $approvedByAll = $this->approved_by_admin && $this->approvedByPartner();

        return $this->type === self::TYPE_SERVICE
            ? $approvedByAll && $this->serviceCompletedAtHasArrived()
            : $approvedByAll;
    }
}
