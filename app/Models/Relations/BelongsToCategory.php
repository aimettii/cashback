<?php


namespace App\Models\Relations;


use App\Models\Category;

trait BelongsToCategory
{
    /**
     * @property $this \Illuminate\Database\Eloquent\Model
     */
    public function category()
    {
        return $this->belongsTo(Category::class)->withDefault([new Category()]);
    }
}
