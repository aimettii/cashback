<?php


namespace App\Models\Relations;


use App\Models\Balance;
use App\Models\Customer;
use App\Models\Partner;
use App\User;

trait HasOneBalance
{
    /**
     * @property $this \Illuminate\Database\Eloquent\Model
     */
    public function balance()
    {
        return $this->hasOne(Balance::class);
    }
}
