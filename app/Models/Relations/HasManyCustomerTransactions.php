<?php


namespace App\Models\Relations;


use App\Models\CustomerTransaction;

trait HasManyCustomerTransactions
{
    /**
     * @property $this \Illuminate\Database\Eloquent\Model
     */
    public function customerTransactions()
    {
        return $this->hasMany(CustomerTransaction::class);
    }
}
