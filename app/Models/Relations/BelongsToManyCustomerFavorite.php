<?php


namespace App\Models\Relations;

use App\Models\Customer;

trait BelongsToManyCustomerFavorite
{
    /**
     * @property $this \Illuminate\Database\Eloquent\Model
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function customerFavorites()
    {
        return $this->belongsToMany(Customer::class, 'customer_favorite_offer');
    }
}
