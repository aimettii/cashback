<?php


namespace App\Models\Relations;


use App\Models\Customer;
use App\Models\Partner;
use App\User;

trait HasOneCustomer
{
    /**
     * @property $this \Illuminate\Database\Eloquent\Model
     */
    public function customer()
    {
        return $this->hasOne(Customer::class);
    }
}
