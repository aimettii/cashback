<?php


namespace App\Models\Relations;


use App\Models\Offer;

trait HasOneOffer
{
    public function offer()
    {
        return $this->hasOne(Offer::class);
    }
}