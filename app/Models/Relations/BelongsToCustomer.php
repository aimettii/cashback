<?php


namespace App\Models\Relations;


use App\Models\Customer;

trait BelongsToCustomer
{
    /**
     * @property $this \Illuminate\Database\Eloquent\Model
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class)->withDefault([new Customer()]);
    }
}
