<?php


namespace App\Models\Relations;


use App\Models\Partner;

trait BelongsToPartner
{
    /**
     * @property $this \Illuminate\Database\Eloquent\Model
     */
    public function partner()
    {
        return $this->belongsTo(Partner::class)->withDefault([new Partner()]);
    }
}
