<?php


namespace App\Models\Relations;


use App\Models\Partner;
use App\User;

trait HasOnePartner
{
    /**
     * @property $this \Illuminate\Database\Eloquent\Model
     */
    public function partner()
    {
        return $this->hasOne(Partner::class);
    }
}
