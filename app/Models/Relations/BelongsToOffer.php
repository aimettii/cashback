<?php


namespace App\Models\Relations;


use App\Models\Offer;

trait BelongsToOffer
{
    public function offer()
    {
        return $this->belongsTo(Offer::class);
    }
}