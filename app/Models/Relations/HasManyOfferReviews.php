<?php


namespace App\Models\Relations;


use App\Models\Review;

trait HasManyOfferReviews
{
    public function offerReviews()
    {
        return $this->hasMany(Review::class);
    }
}