<?php


namespace App\Models\Relations;


use App\Models\Offer;

trait HasManyOffers
{
    /**
     * @property $this \Illuminate\Database\Eloquent\Model
     */
    public function offers()
    {
        return $this->hasMany(Offer::class);
    }
}
