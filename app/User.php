<?php

namespace App;

use App\Models\Partner;
use App\Models\Relations\HasOneBalance;
use App\Models\Relations\HasOneCustomer;
use App\Models\Relations\HasOnePartner;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    const TYPE_ADMIN = 1;
    const TYPE_CUSTOMER = 2;
    const TYPE_PARTNER = 3;

    use Notifiable;
    use HasOneCustomer, HasOnePartner, HasOneBalance; // Relations
    use HasApiTokens; // Sanctum

    protected $guarded = [];
    protected $hidden = ['password', 'remember_token',];
    protected $casts = ['email_verified_at' => 'datetime',];

    public function isCustomer()
    {
        return $this->type === self::TYPE_CUSTOMER;
    }

    public function isAdmin()
    {
        return $this->type === self::TYPE_ADMIN;
    }

    public function isPartner()
    {
        return $this->type === self::TYPE_PARTNER && $this->partner instanceof Partner;
    }

    public static function scopeCustomers(Builder $query)
    {
        return $query->where('type', self::TYPE_CUSTOMER);
    }

    public static function scopePartners(Builder $query)
    {
        return $query->where('type', self::TYPE_PARTNER);
    }

    public static function scopeAdmins(Builder $query)
    {
        return $query->where('type', self::TYPE_ADMIN);
    }
}
