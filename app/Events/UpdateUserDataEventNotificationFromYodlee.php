<?php

namespace App\Events;

use App\Models\Customer;
use Carbon\Carbon;
use Cassandra\Custom;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class UpdateUserDataEventNotificationFromYodlee
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $loginName;
    public $fromDate;
    public $toDate;
    public $eventNotificationId;
    public $relatedCustomer;

    /**
     * Create a new event instance.
     *
     * @param Customer $relatedCustomer
     * @param $loginName
     * @param $fromDate
     * @param $toDate
     */
    public function __construct(Customer $relatedCustomer, $loginName, $fromDate, $toDate)
    {
        $this->loginName = $loginName;
        $this->fromDate = $fromDate;
        $this->toDate = $toDate;
        $this->relatedCustomer = $relatedCustomer;

        $eventNotificationId = DB::table('yodlee_event_notifications')->insertGetId([
            'login_name' => $loginName,
            'from_date' => $fromDate,
            'to_date' => $toDate,
            'handled' => false,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $this->eventNotificationId = $eventNotificationId;
    }
}
