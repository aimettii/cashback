<?php

namespace App\Events;

use App\Models\Customer;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NewYodleeTransaction
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $potentialTransactionEntity;
    public $relatedCustomer;
    public $notificationId;

    /**
     * Event when getting new transaction from Yodlee API.
     *
     * @param Customer $relatedCustomer
     * @param $potentialTransactionEntity https://developer.yodlee.com/Yodlee_API/docs/v1_1/Data_Model/Resource_Transactions#Transactions
     * @param null $notificationId
     */
    public function __construct(Customer $relatedCustomer, $potentialTransactionEntity, $notificationId = null)
    {
        $this->potentialTransactionEntity = $potentialTransactionEntity;
        $this->relatedCustomer = $relatedCustomer;
        $this->notificationId = $notificationId;
    }
}
