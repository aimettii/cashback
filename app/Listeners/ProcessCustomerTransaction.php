<?php

namespace App\Listeners;

use App\Events\CustomerTransactionApproved;
use App\Jobs\ProcessTransaction;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProcessCustomerTransaction implements ShouldQueue
{
    /**
     * @param CustomerTransactionApproved $event
     */
    public function handle(CustomerTransactionApproved $event)
    {
        ProcessTransaction::dispatch($event->transaction);
    }
}
