<?php

namespace App\Listeners;

use App\Events\NewYodleeTransaction;
use App\Events\UpdateUserDataEventNotificationFromYodlee;
use App\Facades\Yodlee;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class HandleWithUpdatedUserDataFromYodlee implements ShouldQueue
{
    /**
     * Extract DATA_UPDATES.
     * https://developer.yodlee.com/docs/api/1.1/DataExtracts
     *
     * @param UpdateUserDataEventNotificationFromYodlee $event
     * @return void
     * @throws \Exception
     */
    public function handle(UpdateUserDataEventNotificationFromYodlee $event)
    {
        $userData = Yodlee::dataExtract($event->loginName, $event->fromDate, $event->toDate);

        if (!$userData) {
            throw new \Exception('Data extracting fail: ' . `$event->loginName $event->fromDate $event->toDate`);
        }

        Log::info('Data extracted: ' . json_encode($userData));

        foreach ($userData as $item) {
            if (isset($item->transaction)) {
                foreach ($item->transaction as $potentialTransactionEntity) {
                    event(new NewYodleeTransaction($event->relatedCustomer, $potentialTransactionEntity, $event->eventNotificationId));
                }
            }

            if (isset($item->providerAccount)) {
                // handle providerAccount
            }

            if (isset($item->holding)) {
                // handle Holding
            }

            if (isset($item->account)) {
                // handle Account
            }
        }

        if ($event->eventNotificationId) {
            DB::table('yodlee_event_notifications')->where('id', $event->eventNotificationId)->update([
                    'handled' => true,
                    'data_extracted' => json_encode($userData),
                    'updated_at' => Carbon::now()
                ]);
        }
    }

    /**
     * Handle a job failure.
     *
     * @param  \App\Events\UpdateUserDataEventNotificationFromYodlee  $event
     * @param  \Throwable  $exception
     * @return void
     */
    public function failed(UpdateUserDataEventNotificationFromYodlee $event, $exception)
    {
        if ($event->eventNotificationId) {
            DB::table('yodlee_event_notifications')->where('id', $event->eventNotificationId)->update([
                'failed' => true,
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
