<?php

namespace App\Listeners;

use App\Models\Balance;

class CreateBalanceRelation
{
    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     * @throws \Exception
     */
    public function handle($event)
    {
        if (($event->user->customer || $event->user->partner) && !$event->user->balance) {
            Balance::register($event->user);
        }
    }
}
