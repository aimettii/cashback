<?php

namespace App\Listeners;

use App\Events\CustomerTransactionApprovalRequired;
use App\Jobs\ProcessTransaction;
use App\Models\Partner;
use App\Notifications\ApproveTransaction;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class ApprovalNoticeCustomerTransaction implements ShouldQueue
{
    /**
     * @param CustomerTransactionApprovalRequired $event
     */
    public function handle(CustomerTransactionApprovalRequired $event)
    {
        $transaction = $event->transaction;
        $notifiable = $transaction->approvedByPartner()
            ? User::admins()->get()
            : $transaction->partner->user;

        if (is_a($notifiable, Collection::class)) {
            $link = route('admin.transactions.approve', ['id' => $transaction->id]);
            $notifiable->each(function ($user) use ($event, $link) {
                Notification::send($user, new ApproveTransaction($event->transaction, $link));
            });
        }

        if (is_a($notifiable, User::class)) {
            $link = route('partner.transactions.approve', ['id' => $transaction->id]);
            Notification::send($notifiable, new ApproveTransaction($event->transaction, $link));
        }
    }
}
