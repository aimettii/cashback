<?php

namespace App\Listeners;

use App\Facades\Yodlee;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class RegisterYodleeUserForCustomer implements ShouldQueue
{
    use InteractsWithQueue;
    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     * @throws \Exception
     */
    public function handle($event)
    {
        $yodlee = Yodlee::getInstance();
        $yodlee->registerUser($event->customer);

        if ($yodlee->registeredUser) {
            $event->customer->update(['yodlee_login_name' => $yodlee->registeredUser->loginName]);
        } else {
            throw new \Exception('Fail while registering user: ' . $yodlee->failWhileRegisteringUser);
        }
    }
}
