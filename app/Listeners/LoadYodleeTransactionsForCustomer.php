<?php

namespace App\Listeners;

use App\Events\NewYodleeTransaction;
use App\Facades\Yodlee;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class LoadYodleeTransactionsForCustomer implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     * @throws \Exception
     */
    public function handle($event)
    {
        $transactions = Yodlee::getTransactions($event->customer->yodlee_login_name, '2010-01-01');

        if (!$transactions) {
            throw new \Exception('Fail while loading transactions');
        }

        foreach ($transactions as $transaction) {
            event(new NewYodleeTransaction($event->customer, $transaction));
        }
    }
}
