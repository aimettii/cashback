<?php

namespace App\Listeners;

use App\Notifications\NewTransaction;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

class SendNewTransactionNotification
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $event->transaction->customer->user->notify(new NewTransaction($event->transaction));
        $event->transaction->partner->user->notify(new NewTransaction($event->transaction));
        Notification::send(User::admins()->get(), new NewTransaction($event->transaction));
    }
}
