<?php

namespace App\Listeners;

use App\Events\CustomerTransactionCreated;
use App\Models\CustomerTransaction;
use App\Models\Partner;
use App\Services\types\YodleeTransactionEntity;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CreateCustomerTransactionFromYodleeEntity implements ShouldQueue
{
    const CURRENCY_RATIO = 1.4;

    /**
     * Handle the event.
     *
     * https://developer.yodlee.com/Yodlee_API/docs/v1_1/Data_Model/Resource_Transactions#Transactions
     *
     * @param  \App\Events\NewYodleeTransaction  $event
     * @return void|bool
     */
    public function handle($event)
    {
        // @todo Validate transaction entity
        Log::info('Potential transaction handling: ' . json_encode($event->potentialTransactionEntity));
        $existCustomerTransaction = CustomerTransaction::where([
            ['customer_id', $event->relatedCustomer->id],
            ['yodlee_unique_id', $event->potentialTransactionEntity->id]
        ])->first();

        if (! is_null($existCustomerTransaction)) {
            Log::info('Transaction already created, exit: ' . $existCustomerTransaction->id);
            return false; // Stop propagation
        }

        $partnerByMerchant =  isset($event->potentialTransactionEntity->merchant->name)
            ? Partner::whereRaw('LOWER(merchant_name) = ?', [strtolower($event->potentialTransactionEntity->merchant->name)])->first()
            : null;

        if ($partnerByMerchant) {
            Log::info('Transaction associated with partner: ' . $partnerByMerchant->id);
        }

        $valid = (
            (!isset($event->potentialTransactionEntity->isDeleted) || !$event->potentialTransactionEntity->isDeleted) &&
            isset($event->potentialTransactionEntity->baseType) && $event->potentialTransactionEntity->baseType === YodleeTransactionEntity::BAST_TYPE_DEBIT &&
            isset($event->potentialTransactionEntity->status) && $event->potentialTransactionEntity->status === YodleeTransactionEntity::STATUS_POSTED &&
            isset($event->potentialTransactionEntity->amount) &&
            isset($event->potentialTransactionEntity->amount->currency) &&
            in_array($event->potentialTransactionEntity->amount->currency, ['USD', 'AUD']) &&
            $partnerByMerchant !== null
        );

        if ($valid) {
            $activeOffer = $partnerByMerchant->getActiveOffer(Carbon::parse($event->potentialTransactionEntity->createdDate));

            $type = NULL;

            if (is_null($activeOffer)) {
                Log::warning('Not found active offers for this transaction date');
            } else {
               $type = $activeOffer->type;
            }

            $amountInAUD = $event->potentialTransactionEntity->amount->amount;

            if ($event->potentialTransactionEntity->amount->currency === 'USD') {
                $amountInAUD = $amountInAUD * self::CURRENCY_RATIO; // @todo Get available currency e.g google
            }

            $createdTransaction = CustomerTransaction::create([
                'customer_id' => $event->relatedCustomer->id,
                'partner_id' => $partnerByMerchant->id,
                'yodlee_unique_id' => $event->potentialTransactionEntity->id,
                'created_at' => $event->potentialTransactionEntity->createdDate,
                'value' => $amountInAUD,
                'data' => json_encode($event->potentialTransactionEntity),
                'type' => $type
            ]);

            if ($type === CustomerTransaction::TYPE_RETAIL) {
                $createdTransaction->generateReleasedAt();
            }

            event(new CustomerTransactionCreated($createdTransaction));

            Log::info('Transaction valid and was created(' . $type . '): ' . $createdTransaction->id);
        } else {
            Log::info('Transaction not valid: ' . json_encode($event->potentialTransactionEntity));
            return false; // Stop propagation
        }
    }

    /**
     * Handle a job failure.
     *
     * @param  \App\Events\NewYodleeTransaction  $event
     * @param  \Throwable  $exception
     * @return void
     */
    public function failed($event, $exception)
    {
        if ($event->notificationId) {
            DB::table('yodlee_event_notifications')->where('id', $event->notificationId)->update([
                'failed' => true,
            ]);
        }
    }
}
