<?php

namespace App\Console\Commands;

use App\Jobs\ProcessTransaction;
use App\Models\CustomerTransaction;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class TransactionUpdateRefundStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transaction:update-refund-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $pendingTransactions = CustomerTransaction::pending()
            ->whereNotNull('released_at')
            ->where('released_at', '<', Carbon::now())
            ->where(function ($query) {
                return $query->whereNotNull('service_completed_at')
                    ->orWhere('type', CustomerTransaction::TYPE_RETAIL);
            })
            ->get();

        if ($pendingTransactions->count() === 0) {
            return $this->warn('No transactions ready for status change found');
        }

        $this->info('Ready to change transaction status: ' . $pendingTransactions->count());
        $this->info('Dispatching... ');

        foreach ($pendingTransactions as $transaction) {
            ProcessTransaction::withChain([
                function () use ($transaction) {
                    $transaction->update([
                        'status' => CustomerTransaction::STATUS_COMPLETED,
                        'released_at' => Carbon::now()
                    ]);
                },
            ])->dispatch($transaction);
        }

        $this->info('Jobs in queue.');
    }
}
