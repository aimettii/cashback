<?php

namespace App\Providers;

use App\Models\Customer;
use App\Models\Partner;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('access-admin', function ($user) {
            return $user->isAdmin();
        });

        Gate::define('access-partner', function ($user) {
            return $user->isPartner();
        });

        Gate::define('access-admin-partner', function ($user) {
            return $user->isAdmin() || $user->isPartner();
        });

        Gate::define('access-customer', function ($user) {
            return $user->isCustomer() && $user->customer instanceof Customer;
        });
    }
}
