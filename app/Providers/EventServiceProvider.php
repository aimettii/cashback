<?php

namespace App\Providers;

use App\Events\CustomerLinkedProvider;
use App\Events\CustomerTransactionApprovalRequired;
use App\Events\CustomerTransactionApproved;
use App\Events\CustomerTransactionCreated;
use App\Events\NewYodleeTransaction;
use App\Events\RegisteredCustomer;
use App\Events\UpdateUserDataEventNotificationFromYodlee;
use App\Listeners\ApprovalNoticeCustomerTransaction;
use App\Listeners\CreateBalanceRelation;
use App\Listeners\CreateCustomerTransactionFromYodleeEntity;
use App\Listeners\HandleWithUpdatedUserDataFromYodlee;
use App\Listeners\LoadYodleeTransactionsForCustomer;
use App\Listeners\ProcessCustomerTransaction;
use App\Listeners\RegisterYodleeUserForCustomer;
use App\Listeners\SendNewTransactionNotification;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            CreateBalanceRelation::class,
            SendEmailVerificationNotification::class,
        ],
        RegisteredCustomer::class => [
            RegisterYodleeUserForCustomer::class
        ],
        CustomerTransactionCreated::class => [
            SendNewTransactionNotification::class
        ],
        UpdateUserDataEventNotificationFromYodlee::class => [
            HandleWithUpdatedUserDataFromYodlee::class
        ],
        NewYodleeTransaction::class => [
            CreateCustomerTransactionFromYodleeEntity::class
        ],
        CustomerLinkedProvider::class => [
            LoadYodleeTransactionsForCustomer::class
        ],
        CustomerTransactionApproved::class => [
            ProcessCustomerTransaction::class,
        ],
        CustomerTransactionApprovalRequired::class => [
            ApprovalNoticeCustomerTransaction::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
