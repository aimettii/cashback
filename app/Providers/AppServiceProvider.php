<?php

namespace App\Providers;

use App\Services\YodleeIntegration;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        require_once(app_path() . '/Helpers/helpers.php');

        $this->app->bind('yodlee', function ($app) {
            return new YodleeIntegration();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
