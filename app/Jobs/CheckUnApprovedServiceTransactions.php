<?php

namespace App\Jobs;

use App\Events\CustomerTransactionApprovalRequired;
use App\Models\CustomerTransaction;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class CheckUnApprovedServiceTransactions implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $transactions = CustomerTransaction::pending()
            ->unApprovedByAdminWithTypeService()
            ->get();

        if ($transactions->count()) {
            $transactions->each(function ($transaction) {
                event(new CustomerTransactionApprovalRequired($transaction));
            });
        }
    }
}
