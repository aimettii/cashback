<?php

namespace App\Jobs;

use App\Models\CustomerTransaction;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ProcessTransaction implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $transaction;

    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @param CustomerTransaction $transaction
     */
    public function __construct(CustomerTransaction $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        Log::debug('Process transaction job, transaction_id:' . $this->transaction->id);
        if ($this->transaction->status !== CustomerTransaction::STATUS_PENDING) {
            Log::error('Status for transaction is not a "pending"');
            throw new \Exception('Status for transaction is not a "pending"');
        }

        $balanceOfPartner = $this->transaction->partner->user->balance;
        $balanceOfCustomer = $this->transaction->customer->user->balance;

        $activeOffer = $this->transaction->partner->getActiveOffer($this->transaction->created_at);

        if (is_null($activeOffer)) {
            Log::warning('Not found active offers for this transaction date');
            throw new \Exception('Not found active offers for this transaction date');
        }

        Log::info("Balance_id({$balanceOfPartner->id}) of partner before change status has value: {$balanceOfPartner->value}");
        Log::info("Balance_id({$balanceOfCustomer->id}) of customer before change status has value: {$balanceOfCustomer->value}");

        $cashbackPercent = $activeOffer->percent;

        Log::info("Active offer({$activeOffer->id}), Cashback percent: {$activeOffer->percent}");

        $cashbackValue = $this->transaction->value * $cashbackPercent / 100;

        if ($balanceOfPartner->value < $cashbackValue) {
            Log::warning(
                "The partner's balance is less than the cashback amount! Balance of partner amount: {$balanceOfPartner}, Cashback value: {$cashbackValue}");
            return;
        }

        Log::info("Transaction purchase amount: {$this->transaction->value}, Cashback value: {$cashbackValue}");

        $balanceOfPartner->subtract($cashbackValue); 
        $balanceOfCustomer->add($cashbackValue);

        Log::info("Balance_id({$balanceOfPartner->id}) of partner after change status has value: {$balanceOfPartner->value}");
        Log::info("Balance_id({$balanceOfCustomer->id}) of customer after change status has value: {$balanceOfCustomer->value}");
    }
}
