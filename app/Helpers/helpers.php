<?php

if (! function_exists('getFakeImageUrl')) {
    /**
     * @param int $size
     * @param bool $byWidth
     * @param float|null $ratio
     * @return string
     */
    function getFakeImageUrl(int $size, bool $byWidth = true, float $ratio = null): string
    {
        $ratio = $ratio ?? rand(5, 25) / 10;

        if ($byWidth) {
            $width = $size;
            $height = round($width / $ratio);
        } else {
            $height = $size;
            $width = round($height * $ratio);
        }

        return 'https://placeimg.com/' . $width . '/' . $height. '/any?random=' . rand(1,1000);
    }
}
