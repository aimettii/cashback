<?php


namespace App\Services;

use App\Models\Customer;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class YodleeIntegration
{
    const HEADER_API_VERSION = '1.1';

    const GET_TOKEN_ROUTE = '/auth/token';
    const USER_REGISTER_ROUTE = '/user/register';
    const CONFIGS_NOTIFICATIONS_EVENTS_ENDPOINT = '/configs/notifications/events';
    const DATA_EXTRACT_USER_DATA__ROUTE = '/dataExtracts/userData';
    const TRANSACTIONS_ENDPOINT = '/transactions';
    const ACCOUNTS_ENDPOINT = '/accounts';

    protected $http;

    public $userToken;
    public $registeredUser;
    public $failWhileRegisteringUser;

    public function getInstance()
    {
        return $this;
    }

    public function generateUserToken($loginName)
    {
        Log::debug('Generating yodlee token for: ' . $loginName);

        $response = Http::asForm()->withHeaders([
            'loginName' => $loginName,
            'Api-Version' => self::HEADER_API_VERSION,
        ])->post(env('YODLEE_API_URL') . self::GET_TOKEN_ROUTE, [
            'clientId' => env('YODLEE_CLIENT_ID'),
            'secret' => env('YODLEE_SECRET'),
        ]);

        $responseBody = json_decode($response->body());

        if ($response->successful()) {
            if (isset($responseBody->token, $responseBody->token->accessToken, $responseBody->token->issuedAt, $responseBody->token->expiresIn)) {
                Log::info('Success generated yodlee token, response: ' . $response->body());
                $this->userToken = $responseBody->token;
                return $this->userToken;
            }
            Log::error('Fail while generation user token, bad response returned: ' . $response->body());
            return false;
        } else {
            Log::error('Fail while generation user token, response: ' . $response->body());
            return false;
        }
    }

    /**
     * Registering user on yodlee portal
     *
     * @param Customer $customer
     * @return bool|mixed
     * @throws \Exception
     */
    public function registerUser(Customer $customer)
    {
        Log::debug('RegisteringYodleeUser for customer_id:' . $customer->id);

        $params = [
            'user' => [
                'loginName' => Customer::generateYodleeLoginName(),
                'email' => $customer->user->email,
                'name' => [
                    'first' => $customer->first_name,
                    'last' => $customer->last_name,
                ],
//                'address' => [
//
//                ],
                'preferences' => [
                    'currency' => 'AUD',
                    'timeZone' => 'UTC',
                    'dateFormat' => 'YYYY-MMM-DD',
                    'locale' => 'en_US',
                ]
            ]
        ];

        $adminToken = $this->generateAdminToken();

        if (!$adminToken) {
            Log::error('Fail while register user');
            return false;
        }

        $response = Http::withHeaders([
            'Api-Version' => self::HEADER_API_VERSION,
        ])
            ->withToken($adminToken->accessToken)
            ->withBody(json_encode($params), 'application/json')
            ->post(env('YODLEE_API_URL') . self::USER_REGISTER_ROUTE);

        $responseBody = json_decode($response->body());

        if ($response->successful()) {
            if (isset($responseBody->user, $responseBody->user->id, $responseBody->user->loginName)) {
                Log::info('Success registered yodlee user: ' . $response->body());
                $this->registeredUser = $responseBody->user;
                return $this->registeredUser;
            }
            $this->failWhileRegisteringUser = $response->body();
            Log::error('Fail while registering user, response: ' . $response->body());
            return false;
        } else {
            $this->failWhileRegisteringUser = $response->body();
            Log::error('Fail while registering user, response: ' . $response->body());
            return false;
        }
    }

    /**
     * Register DATA_UPDATES event notification of the Yodlee webhook
     *
     * https://developer.yodlee.com/docs/api/1.1/Webhooks
     *
     * @param string $eventName
     * @return bool
     */
    public function registerEventNotification($eventName = 'DATA_UPDATES')
    {
        Log::info('Registering event notification...');
        $adminToken = $this->generateAdminToken();

        if (!$adminToken) {
            Log::error('Fail while registering event notification');
            return false;
        }

        $params = [
            'event' => [
                'callbackUrl' => env('YODLEE_NOTIFICATIONS_CALLBACK_URL', route('yodlee-webhook'))
            ]
        ];

        $response = Http::withHeaders([
            'Api-Version' => self::HEADER_API_VERSION,
        ])
            ->withToken($adminToken->accessToken)
            ->withBody(json_encode($params), 'application/json')
            ->post(env('YODLEE_API_URL') . self::CONFIGS_NOTIFICATIONS_EVENTS_ENDPOINT . '/' . $eventName);

        $responseBody = json_decode($response->body());

        if ($response->successful()) {
            return true;
        } else {
            Log::error('Fail while registering event notification: ' . $response->body());
            return false;
        }
    }

    public function dataExtract($loginName, $fromDate, $toDate)
    {
        Log::info('Data extracting...');
        $adminToken = $this->generateAdminToken();

        if (!$adminToken) {
            Log::error('Fail while data extracting');
            return false;
        }

        $params = [
            'loginName' => $loginName,
            'fromDate' => $fromDate,
            'toDate' => $toDate,
        ];

        $response = Http::withHeaders([
            'Api-Version' => self::HEADER_API_VERSION,
        ])
            ->withToken($adminToken->accessToken)
            ->get(env('YODLEE_API_URL') . self::DATA_EXTRACT_USER_DATA__ROUTE, $params);

        $responseBody = json_decode($response->body());

        if ($response->successful()) {
            return isset($responseBody->userData) ? $responseBody->userData : json_decode('{}');
        } else {
            Log::error('Fail while data extracting: ' . $response->body());
            return false;
        }
    }

    /**
     *
     * Get Transaction by period
     * ex. fromDate=2018-01-05&toDate=2019-01-01
     * By default, this service returns the last 30 days of transactions from today's date.
     * https://developer.yodlee.com/api-reference#!/transactions/getTransactions
     *
     * @param $loginName
     * @param null $fromDate
     * @param null $toDate
     * @return bool|mixed
     */
    public function getTransactions($loginName, $fromDate = null, $toDate = null)
    {
        Log::info('Transactions extracting...');
        $userToken = $this->generateUserToken($loginName);

        if (!$userToken) {
            Log::error('Fail while transactions extracting');
            return false;
        }

        $params = [];

        if ($fromDate) {
            $params['fromDate'] = $fromDate;
        }

        if ($toDate) {
            $params['toDate'] = $toDate;
        }

        $response = Http::withHeaders([
            'Api-Version' => self::HEADER_API_VERSION,
        ])
            ->withToken($userToken->accessToken)
            ->get(env('YODLEE_API_URL') . self::TRANSACTIONS_ENDPOINT, $params);

        $responseBody = json_decode($response->body());

        if ($response->successful()) {
            return isset($responseBody->transaction) ? $responseBody->transaction : json_decode('[]');
        } else {
            Log::error('Fail while data extracting: ' . $response->body());
            return false;
        }
    }

    public function getLinkedBankAccounts($loginName)
    {
        Log::info('Linked accounts retrieving...');
        $userToken = $this->generateUserToken($loginName);

        if (!$userToken) {
            Log::error('Fail while for accounts retrieving');
            return false;
        }

        $response = Http::withHeaders([
            'Api-Version' => self::HEADER_API_VERSION,
        ])
            ->withToken($userToken->accessToken)
            ->get(env('YODLEE_API_URL') . self::ACCOUNTS_ENDPOINT);

        $responseBody = json_decode($response->body());

        if ($response->successful()) {
            return isset($responseBody->account) ? $responseBody->account : [];
        } else {
            Log::error('Fail while for accounts retrieving: ' . $response->body());
            return false;
        }
    }

    public function deleteLinkedBankAccount($loginName, $accountId)
    {
        Log::info('Delete Linked account ' . $accountId . ' beginning...');
        $userToken = $this->generateUserToken($loginName);

        if (!$userToken) {
            Log::error('Fail while for deleting account');
            return false;
        }

        $response = Http::withHeaders([
            'Api-Version' => self::HEADER_API_VERSION,
        ])
            ->withToken($userToken->accessToken)
            ->delete(env('YODLEE_API_URL') . self::ACCOUNTS_ENDPOINT . '/' . $accountId);

        if ($response->successful()) {
            return true;
        } else {
            Log::error('Fail while for accounts retrieving: ' . $response->body());
            return false;
        }
    }

    public function generateAdminToken()
    {
        Log::info('Generating admin token...');
        return $this->generateUserToken(env('YODLEE_ADMIN_LOGIN_NAME'));
    }
}