<?php

namespace App\Services\types;

/**
 * https://developer.yodlee.com/Yodlee_API/docs/v1_1/Data_Model/Resource_Transactions#Transactions
 */
class YodleeTransactionEntity {
    const BAST_TYPE_CREDIT = 'CREDIT'; //Denotes that the transactions are being credited to the account.
    const BAST_TYPE_DEBIT  = 'DEBIT'; //Denotes that the transactions are getting debited to the account.

    const STATUS_POSTED = 'POSTED'; // Denotes that the transaction is realized on the account's balance.
    const STATUS_PENDING = 'PENDING'; // Denotes that the transaction is not yet realized on the account's balance.
    const STATUS_SCHEDULED = 'SCHEDULED'; // Denotes that the transacation is a future dated transaction.
    const STATUS_FAILED = 'FAILED'; // Denotes the failed transactions. It is only applicable to rejected claims in a Flexible Spending account.
}
