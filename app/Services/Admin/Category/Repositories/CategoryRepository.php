<?php


namespace App\Services\Admin\Category\Repositories;


use App\Models\Category;

class CategoryRepository
{
    /**
     * @return Category[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return Category::all();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getItem(int $id)
    {
        return Category::findOrFail($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return Category::create($data);
    }

    /**
     * @param Category $category
     * @param array $data
     * @return Category|null
     */
    public function update(Category $category, array $data)
    {
        $category->update($data);

        return $category->fresh();
    }

    /**
     * @param int $id
     * @return int
     */
    public function destroy(int $id)
    {
        return Category::destroy($id);
    }
}
