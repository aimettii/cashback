<?php


namespace App\Services\Admin\Category;


use App\Services\Admin\Category\Repositories\CategoryRepository;

class CategoryService
{
    /**
     * @var CategoryRepository $repository
     */
    private $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return \App\Models\Category[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return $this->repository->index();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function show(int $id)
    {
        return $this->repository->getItem($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->repository->store($data);
    }

    /**
     * @param int $id
     * @param array $data
     * @return \App\Models\Category|null
     */
    public function update(int $id, array $data)
    {
        $item = $this->repository->getItem($id);

        return $this->repository->update($item, $data);
    }

    /**
     * @param int $id
     * @return int
     */
    public function destroy(int $id)
    {
        return $this->repository->destroy($id);
    }
}
