<?php

namespace App\Notifications;

use App\Models\CustomerTransaction;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewTransaction extends Notification implements ShouldQueue
{
    use Queueable;

    public $transaction;

    /**
     * Create a new notification instance.
     *
     * @param CustomerTransaction $transaction
     */
    public function __construct(CustomerTransaction $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     * @throws \Exception
     */
    public function toMail(User $notifiable)
    {
        switch ($notifiable->type) {
            case User::TYPE_ADMIN;
                return (new MailMessage)->markdown(
                    'mail.admin.new-transaction-notification', ['transaction' => $this->transaction]
                );
            break;
            case User::TYPE_CUSTOMER;
                return (new MailMessage)->markdown(
                    'mail.customer.new-transaction-notification', ['transaction' => $this->transaction]
                );
                break;
            case User::TYPE_PARTNER;
                return (new MailMessage)->markdown(
                    'mail.partner.new-transaction-notification', ['transaction' => $this->transaction]
                );
                break;
            default:
                throw new \Exception('WTF?');
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
