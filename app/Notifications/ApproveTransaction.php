<?php

namespace App\Notifications;

use App\Models\CustomerTransaction;
use App\Models\Partner;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ApproveTransaction extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var CustomerTransaction
     */
    private $transaction;

    /**
     * @var string
     */
    private $link;

    /**
     * ApproveTransaction constructor.
     * @param CustomerTransaction $transaction
     * @param string $link
     */
    public function __construct(CustomerTransaction $transaction, string $link)
    {
        $this->transaction = $transaction;
        $this->link = $link;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param User $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     * @throws \Exception
     */
    public function toMail(User $notifiable)
    {
        return (new MailMessage)->markdown(
            'mail.transaction.approve-transaction-notification', [
                'transaction' => $this->transaction,
                'link' => $this->link
            ]
        );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
