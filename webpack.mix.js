// const mix = require('laravel-mix');
// require('laravel-mix-merge-manifest');
//
// /*
//  |--------------------------------------------------------------------------
//  | Mix Asset Management
//  |--------------------------------------------------------------------------
//  |
//  | Mix provides a clean, fluent API for defining some Webpack build steps
//  | for your Laravel application. By default, we are compiling the Sass
//  | file for the application as well as bundling up all the JS files.
//  |
//  */
// mix.webpackConfig({
//   resolve: {
//     alias: {
//       '@': path.resolve(__dirname, 'resources/js')
//     }
//   },
//   optimization: {
//     splitChunks: {
//       cacheGroups: {
//         default: false,
//         vendors: {
//           chunks: 'initial',
//           name: 'vendor',
//           filename: 'js/[name].js'
//         }
//       }
//     },
//   },
// })
//   .js('resources/js/app.js', 'public/js')
//   .extract(['vue', 'uikit'])
//   .mergeManifest()
//   .sass('resources/sass/app.scss', 'public/css');

if (['customers', 'dashboard'].includes(process.env.npm_config_section)) {
  require(`${__dirname}/webpack.${process.env.npm_config_section}.mix.js`)
} else {
  console.log(
  '\x1b[41m%s\x1b[0m',
  'Provide correct --section argument to build command: customers, dashboard'
  )
  throw new Error('Provide correct --section argument to build command!')
}
