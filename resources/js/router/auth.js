export default [
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "calena.login" */ '@/pages/auth/Login'),
    meta: {
      guest: true
    }
  },
  {
    path: '/register/customer',
    name: 'register.customer',
    component: () => import(/* webpackChunkName: "calena.register.customer" */ '@/pages/auth/RegisterCustomer'),
    meta: {
      guest: true
    }
  },
  {
    path: '/register/partner',
    name: 'register.partner',
    component: () => import(/* webpackChunkName: "calena.register.partner" */ '@/pages/auth/RegisterPartner'),
    meta: {
      guest: true
    }
  },
  {
    path: '/password/forgot',
    name: 'password.forgot',
    component: () => import(/* webpackChunkName: "calena.password.forgot" */ '@/pages/auth/PasswordForgot'),
    meta: {
      guest: true
    }
  },
  {
    path: '/password/reset',
    name: 'password.reset',
    component: () => import(/* webpackChunkName: "calena.password.reset" */ '@/pages/auth/PasswordReset'),
  },
];
