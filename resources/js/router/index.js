import store from '@/store';

import VueRouter from 'vue-router';
import AuthRoutes from '@/router/auth';
import ErrorRoutes from '@/router/errors';

const router = new VueRouter({
    mode: 'history',
    history: true,
    routes: [
        {
            path: '/',
            name: 'home',
            component: () => import(/* webpackChunkName: "calena.home" */ '@/pages/Home.vue')
        },
        {
            path: '/offers',
            name: 'offers',
            component: () => import(/* webpackChunkName: "calena.offers" */ '@/pages/Offers.vue'),
            beforeEnter: async (to, from, next) => {
                store.dispatch('setStateField', { field: 'footer', value: false })
                next();
            },
            props: true
        },
        {
            path: '/offers/:slug',
            name: 'offer',
            component: () => import(/* webpackChunkName: "calena.offer" */ '@/pages/Offer.vue'),
            props: true
        },
        ...AuthRoutes,
        ...ErrorRoutes,
        {
            path: '*',
            redirect: {
                name: '404'
            }
        }
    ],
    linkExactActiveClass: 'uk-active',
    scrollBehavior (to, from, savedPosition) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve({ x: 0, y: 0, behavior: 'smooth' })
            }, 300)
        })
    }
});

let initEach = true;

router.beforeEach(async (to, from, next) => {
    if (initEach) {
        await store.dispatch('auth/getAccount');
        initEach = false;
    }

    const loggedIn = store.getters['auth/loggedIn'];
    const requireGuest = to.matched.some(record => record.meta.guest);
    if (loggedIn && requireGuest) next('/');
    next();
});

export default router;
