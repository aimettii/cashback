export default [
  {
    path: '/notfound',
    name: '404',
    component: () => import(/* webpackChunkName: "calena.404" */ '@/pages/errors/Notfound'),
  }
];
