import {createNamespacedHelpers} from 'vuex';
const { mapState, mapActions } = createNamespacedHelpers('offers');

export default {
    computed: {
        ...mapState({
            favorites: state => state.favorites
        })
    },
    async created () {
      await this.getFavoritesAction();
    },
    methods: {
        ...mapActions({
            getFavoritesAction: 'getFavorites'
        })
    }
}
