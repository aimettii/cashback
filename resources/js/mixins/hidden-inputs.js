export default {
  data: () => ({
    hidden: {
      password: true,
      passwordConfirmation: true
    }
  }),
  computed: {
    passwordType () {
      return this.hidden.password ? 'password' : 'text';
    },
    passwordAppendIcon () {
      return this.hidden.password  ? 'lock' : 'unlock';
    },
    passwordConfirmationType () {
      return this.hidden.passwordConfirmation ? 'password' : 'text';
    },
    passwordConfirmationAppendIcon () {
      return this.hidden.passwordConfirmation ? 'lock' : 'unlock';
    }
  },
  methods: {
    togglePasswordType () {
      this.$set(this.hidden, 'password', !this.hidden.password);
    },
    togglePasswordConfirmationType () {
      this.$set(this.hidden, 'passwordConfirmation', !this.hidden.passwordConfirmation);
    }
  }
}
