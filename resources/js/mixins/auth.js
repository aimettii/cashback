import { createNamespacedHelpers } from 'vuex';

const { mapState, mapGetters } = createNamespacedHelpers('auth');

export default {
  computed: {
    ...mapState({
      authUser: state => state.authUser
    }),
    ...mapGetters([
      'loggedIn'
    ])
  }
};
