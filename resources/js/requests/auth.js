import axios from "axios";
import { wrapRequest } from "../helpers";

const AccountRequest = (params) => axios.get('/api/account', params);
const LoginRequest = (params) => axios.post('/login', params);
const LogoutRequest = (params) => axios.get('/logout', params);
const RegisterPartnerRequest = (params) => axios.post('/register/partner', params);
const RegisterCustomerRequest = (params) => axios.post('/register/customer', params);
const ForgotPasswordRequest = (params) => axios.post('/password/email', params);
const ResetPasswordRequest = (params) => axios.post('/password/reset', params);

export const wrappedAccountRequest = wrapRequest(AccountRequest);
export const wrappedLoginRequest = wrapRequest(LoginRequest);
export const wrappedLogoutRequest = wrapRequest(LogoutRequest);
export const wrappedRegisterPartnerRequest = wrapRequest(RegisterPartnerRequest);
export const wrappedRegisterCustomerRequest = wrapRequest(RegisterCustomerRequest);
export const wrappedForgotPasswordRequest = wrapRequest(ForgotPasswordRequest);
export const wrappedResetPasswordRequest = wrapRequest(ResetPasswordRequest);
