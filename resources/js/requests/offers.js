import api from '../plugins/api';

import { wrapRequest } from "../helpers";

const OffersRequest = (params) => api.post('/offers', params);
const OfferRequest = (slug) => api.get(`/offers/${slug}`);
const OfferCategoriesRequest = () => api.get('/offers/categories');
const OfferFavoritesRequest = () => api.get('/offers/favorites');
const OfferToggleFavoriteRequest = (id) => api.get(`/offers/${id}/toggle-favorite`);
const OfferReviewsRequest = (id) => api.get(`/offers/${id}/reviews`);
const OfferAddShareRequest = (id) => api.put(`/offers/${id}/share-add`);

export const wrappedOffersRequest = wrapRequest(OffersRequest);
export const wrappedOfferRequest = wrapRequest(OfferRequest);
export const wrappedOfferCategoriesRequest = wrapRequest(OfferCategoriesRequest);
export const wrappedOfferFavoritesRequest = wrapRequest(OfferFavoritesRequest);
export const wrappedOfferToggleFavoriteRequest = wrapRequest(OfferToggleFavoriteRequest);
export const wrappedOfferReviewsRequest = wrapRequest(OfferReviewsRequest);
export const wrappedOfferAddShareRequest = wrapRequest(OfferAddShareRequest);
