"use strict";

// import vuetify from '@/plugins/vuetify';
// import Vuetify from 'vuetify';

import VueRouter from 'vue-router';
import store from '@/store';
import router from '@/router';

// UIkit Framework
import uikit from '@/plugins/uikit';

import GlobalComponents from "@/globalComponents";

require('./bootstrap');

window.Vue = require('vue');
// Vue.use(Vuetify);
Vue.use(GlobalComponents);
Vue.use(VueRouter);

import { extend, ValidationObserver, ValidationProvider } from 'vee-validate'

import vue2Dropzone from 'vue2-dropzone'
import 'vue2-dropzone/dist/vue2Dropzone.min.css'
Vue.component('vueDropzone', vue2Dropzone)

extend('url', {
    validate(value) {
        if (value) {
            return /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(value);
        }

        return false;
    },
    message: 'This value must be a valid URL',
})

if (document.getElementById('front-app')) {
    const App = new Vue({
        el: '#front-app',
        router,
        // vuetify,
        store
    });
}
