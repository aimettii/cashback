import axios from 'axios';
import router from '../router';

const api = axios.create({
  'baseURL': '/api',
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json"
  }
});

api.interceptors.response.use(null, error => {
  let path = '/error';
  switch (error.response.status) {
    case 403: path = '/login'; break;
    case 404: path = '/notfound'; break;
  }
  router.push(path);
  return Promise.reject(error);
});

export default api;
