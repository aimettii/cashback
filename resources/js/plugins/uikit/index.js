import Vue from 'vue';

// UIkit Framework
import UIkit from 'uikit';

// Default Icons
import Icons from 'uikit/dist/js/uikit-icons';

// Custom Icons
// import Icons from '@/plugins/uikit/icons/uikit-icons';
UIkit.use(Icons);
// Vue.prototype.$uikit = UIkit;

export default UIkit;
