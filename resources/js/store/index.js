import Vue from "vue";
import Vuex from "vuex";

import auth from "./modules/auth";
import offers from "./modules/offers";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    auth,
    offers,
  },
  plugins: [],
  state: {
    footer: true
  },
  mutations: {
    setStateField: (state, { field, value }) => {
      if (_.has(state, field)) {
        state[field] = value;
      }
    },
  },
  actions: {
    setStateField: ({ commit }, payload) => { commit('setStateField', payload) },
  },
  getters: {
    // Store in modules
  },
});

export default store;

