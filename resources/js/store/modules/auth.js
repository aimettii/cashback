import {
  wrappedAccountRequest,
  wrappedLogoutRequest,
} from '../../requests/auth'

const state = {
  authUser: {},
}

const mutations = {
  setUser: (state, account) => {
    state.authUser = account;
  }
}

const actions = {
  getAccount: async ({ commit }) => {
    const account = await wrappedAccountRequest();
    commit('setUser', account);
    return account;
  },
  logout: async ({ commit }) => {
    await wrappedLogoutRequest();
    commit('setUser', {});
    return true;
  }
}

const getters = {
  loggedIn: (state) => Boolean(state.authUser?.id),
  userIsAdmin: (state, getters) => getters.loggedIn && state.authUser.type === 1,
  userIsCustomer: (state, getters) => getters.loggedIn && state.authUser.type === 2,
  userIsPartner: (state, getters) => getters.loggedIn && state.authUser.type === 3,
  isGuestOrCustomer: (state, getters) => !getters.loggedIn || getters.userIsCustomer,
}

export default {
  namespaced: true,
  state, getters, mutations, actions
}

