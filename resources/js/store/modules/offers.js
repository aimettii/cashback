import {
    wrappedOffersRequest,
    wrappedOfferRequest,
    wrappedOfferCategoriesRequest,
    wrappedOfferFavoritesRequest,
    wrappedOfferToggleFavoriteRequest
} from "../../requests/offers";

const state = {
    offer: {},
    offers: [],
    categories: [],
    activeCategory: null,
    favorites: [],
    pagination: {
        per_page: 10,
        total: 0,
        current_page: 1,
        from: 0,
        to: 0,
        sort_by: 'id',
        sort_order: 'desc'
    }
};

const mutations = {
    setOffers: (state, offers) => {state.offers = offers},
    updateOffers: (state, offers) => {state.offers = _.uniqBy([...state.offers, ...offers], 'id')},
    setPagination: (state, payload) => {
        state.pagination = { ...state.pagination, ...payload };
    },
    setStateField: (state, { field, value }) => {
        state[field] = value;
    },
    setStateFields: (state, payload) => {
        Object.keys(payload).forEach((field) => {
           if (_.has(state, field)) {
               state[field] = payload[field];
           }
        });
    }
};

const actions = {
    getOffer: async ({ commit }, slug) => {
        const { data } = await wrappedOfferRequest(slug);
        commit('setStateField', { field: 'offer', value: data });
    },
    getCategories: async ({ commit }) => {
        const { data } = await wrappedOfferCategoriesRequest();
        commit('setStateField', { field: 'categories', value: data });
    },
    getOffers: async  ({ commit }, { query = null, increasePage = false } = {}) => {
        const params = {
            current_page: increasePage
                ? state.pagination.current_page + 1
                : 1,
            per_page: state.pagination.per_page,
            sort_order: state.pagination.sort_order,
            sort_by: state.pagination.sort_by,
            category: state.activeCategory,
            query
        }
        const { data, pagination } = await wrappedOffersRequest(params);
        increasePage
            ? commit('updateOffers', data)
            : commit('setOffers', data);
        commit('setPagination', pagination);
    },
    getFavorites: async ({ commit }) => {
        const { data } = await wrappedOfferFavoritesRequest();
        commit('setStateField', { field: 'favorites', value: data });
    },
    toggleFavorite: async ({ commit, state }, id) => {
        let newFavorites = state.favorites.slice(0);
        let favoriteIndex = newFavorites.findIndex(o => o === id)
      
        console.log(favoriteIndex);
        
        if (favoriteIndex > -1) {
          newFavorites.splice(favoriteIndex, 1);
        } else {
          newFavorites.push(id);
        }
        
        commit('setStateField', { field: 'favorites', value: newFavorites });
        const { data: { favorites, offer } } = await wrappedOfferToggleFavoriteRequest(id);
        commit('setStateField', { field: 'offer', value: offer });
        commit('setStateField', { field: 'favorites', value: favorites });
    },
    setPagination: ({ commit }, payload) => {commit('setPagination', payload)},
    setStateField: ({ commit }, payload) => {commit('setStateField', payload)},
    setStateFields: ({ commit }, payload) => {commit('setStateFields', payload)}
};

const getters = {
    isFavorite: state => id => state.favorites.includes(id)
};

export default {
    namespaced: true,
    state, getters, mutations, actions
}
