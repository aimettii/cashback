import App from './App.vue';
import TitleCaption from "@/components/common/typography/TitleCaption";
import UkImage from "@/components/common/image/UkImage";

const GlobalComponents = {
  install(Vue) {
    Vue.component("app", App);
    Vue.component("title-caption", TitleCaption);
    Vue.component("uk-image", UkImage);
  }
};

export default GlobalComponents;
