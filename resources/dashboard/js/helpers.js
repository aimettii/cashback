import store from "./store/core.js";

const handleError = error => {
    if (error.message !== 'Webserver error') {
        store.dispatch("populateAjaxErrors", error);
    }
};

export const wrapRequest = fn => (...params) =>
    fn(...params)
        .then(response => {
          
            // return response;
            if(response && ( response.status === 200 || response.status === 201 || response.status === 204 )){
                if(response.data === ''){
                    return response.status
                }else {
                  
                  return  response.data
                }
            }else if (response && response.status !== 200 && response.status !== 201 && response.status !== 204) {
                throw response;
            }

            if(response.data){
                return response.data;
            }
        })
        .catch(error => {
            let response = error.response;
            let error_message = 'Webserver error';
            if (response) {
                error_message = (response.data.error) ? response.data.error.message : error_message;
            }
            handleError({
                code: null,
                message: error_message
            })

            return response && response.data ? response.data : response;
        });
