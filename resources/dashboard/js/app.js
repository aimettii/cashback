"use strict";

import vuetify from "./plugins/vuetify.js";
import Vuetify from "vuetify";
import VueRouter from "vue-router";
import adminRouter from "./router/adminRouter";
import store from "./store/core";
import customerRouter from "./router/customerRouter";
import partnerRouter from "./router/partnerRouter";
import CustomerDrawerMenu from "./components/CustomerDrawerMenu";
import AdminDrawerMenu from "./components/AdminDrawerMenu";
import PartnerDrawerMenu from "./components/PartnerDrawerMenu";
import FooterComponent from "@/components/layouts/Footer";
import Logo from "@/components/common/Logo";

require('./bootstrap');

window.Vue = require('vue');
Vue.use(Vuetify);
Vue.use(VueRouter);

import { ValidationProvider, ValidationObserver } from 'vee-validate';

Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);
Vue.component('FooterComponent', FooterComponent);
Vue.component('Logo', Logo);

import vue2Dropzone from 'vue2-dropzone'
import 'vue2-dropzone/dist/vue2Dropzone.min.css'
Vue.component('vueDropzone', vue2Dropzone)

import uikit from '@/plugins/uikit';

if (document.getElementById("admin-app")) {
    const App = new Vue({
        el: '#admin-app',
        router: adminRouter,
        vuetify, store,
        components: {
            AdminDrawerMenu
        },
        data: function () {
            return {
                authUser: window.authUser,
                baseContainerWidth: 'calc(100% - 53px)',
                drawerMenuMiniVariant: null,
            }
        },
    });
}

if (document.getElementById("customer-app")) {
    const App = new Vue({
        el: '#customer-app',
        router: customerRouter,
        vuetify, store,
        components: {
            CustomerDrawerMenu
        },
        data: function () {
            return {
                authUser: window.authUser,
                baseContainerWidth: 'calc(100% - 53px)',
                drawerMenuMiniVariant: null,
                defaultBreakPointThresholds: {

                }
            }
        },
        mounted() {
            this.defaultBreakPointThresholds.xs = this.$vuetify.breakpoint.thresholds.xs;
            this.defaultBreakPointThresholds.sm = this.$vuetify.breakpoint.thresholds.sm;
            this.defaultBreakPointThresholds.md = this.$vuetify.breakpoint.thresholds.md;
            this.defaultBreakPointThresholds.lg = this.$vuetify.breakpoint.thresholds.lg;
        }
    });
}

if (document.getElementById("partner-app")) {
    const App = new Vue({
        el: '#partner-app',
        router: partnerRouter,
        vuetify, store,
        components: {
            PartnerDrawerMenu
        },
        data: function () {
            return {
                authUser: window.authUser,
                baseContainerWidth: 'calc(100% - 53px)',
                drawerMenuMiniVariant: null,
            }
        },

    });
}
