import Router from 'vue-router'

const adminRouter = new Router({
    mode: 'history',
    history: true,
    routes: [
        {
            path: '/dashboard',
            name: 'dashboard',
            component: () => import('../views/admin/Home.vue'),
            props: true
        },
        {
            path: '/dashboard/transactions',
            name: 'dashboard.transactions',
            component: () => import('../views/admin/Transactions.vue'),
            props: true
        },
        {
            path: '/dashboard/transactions/:id',
            component: () => import('../views/admin/Transactions/_id.vue'),
            props: true
        },
        {
            path: '/dashboard/customers',
            name: 'dashboard.customers',
            component: () => import('../views/admin/Customers.vue'),
            props: true
        },
        {
            path: '/dashboard/partners',
            name: 'dashboard.partners',
            component: () => import('../views/admin/Partners.vue'),
            props: true
        },
        {
            path: '/dashboard/categories',
            name: 'dashboard.categories',
            component: () => import('../views/admin/Categories.vue')
        }
    ]
});



export default adminRouter;
