import Router from 'vue-router'

const customerRouter = new Router({
    mode: 'history',
    history: true,
    routes: [
        {
            path: '/dashboard',
            name: 'dashboard',
            component: () => import('../views/customer/Home.vue'),
            props: true
        },
        {
            path: '/dashboard/transactions',
            name: 'dashboard.transactions',
            component: () => import('../views/customer/Transactions.vue'),
            props: true
        },
        {
            path: '/dashboard/partners',
            name: 'dashboard.partners',
            component: () => import('../views/customer/Partners.vue'),
            props: true
        },
        {
            path: '/dashboard/account',
            name: 'dashboard.account',
            component: () => import('../views/customer/Account.vue'),
            props: true
        },
        {
            path: '/dashboard/offers',
            name: 'dashboard.offers',
            component: () => import('../views/customer/Offers.vue'),
            props: true
        },
        {
            path: '/dashboard/fast-link',
            name: 'dashboard.fast-link',
            component: () => import('../views/customer/FastLink.vue'),
            props: true
        },
        {
            path: '/dashboard/favorite-offers',
            name: 'dashboard.favorite-offers',
            component: () => import('../views/customer/FavoriteOffers.vue'),
            props: true
        },
        {
            path: '/dashboard/reviews',
            name: 'dashboard.reviews',
            component: () => import('../views/customer/Reviews'),
            props: true
        },
    ]
})
export default customerRouter;
