import Router from 'vue-router'

const partnerRouter = new Router({
    mode: 'history',
    history: true,
    routes: [
        {
            path: '/dashboard',
            name: 'dashboard',
            component: () => import('../views/partner/Home.vue'),
            props: true
        },
        {
            path: '/dashboard/transactions',
            name: 'dashboard.transactions',
            component: () => import('../views/partner/Transactions.vue'),
            props: true,
        },
        {
            path: '/dashboard/transactions/:id',
            component: () => import('../views/partner/Transactions/_id.vue'),
            props: true
        },
        {
            path: '/dashboard/settings',
            name: 'dashboard.settings',
            component: () => import('../views/partner/Settings.vue'),
            props: true
        },
        {
            path: '/dashboard/offers',
            name: 'dashboard.offers',
            component: () => import('../views/partner/Offers.vue'),
            props: true
        },
        {
            path: '/dashboard/offers/create',
            name: 'dashboard.offers.create',
            component: () => import('../views/partner/OfferCreate.vue'),
            props: true
        },
        {
            path: '/dashboard/offers/:id',
            name: 'dashboard.offers.edit',
            component: () => import('../views/partner/OfferEdit.vue'),
            props: true
        },
        {
            path: '/dashboard/reviews',
            name: 'dashboard.reviews',
            component: () => import('../views/partner/Reviews'),
            props: true
        },
    ]
})
export default partnerRouter;
