import {
  wrappedPartnerCreateOfferRequest,
  wrappedPartnerUpdateOfferRequest,
  wrappedPartnerOfferRequest,
  wrappedPartnerOffersRequest,
  wrappedPartnerTransactionsRequest,
  wrappedPartnerBalanceRequest,
  wrappedPartnerConfirmTransactionRequest,
  wrappedPartnerApproveTransactionRequest,
  wrappedPartnerUnApproveTransactionRequest,
  wrappedPartnerTransactionRequest,
  wrappedPartnerConfirmProvisionServiceTransactionRequest,
} from "../requests/partner";
import {
  wrappedCategoriesRequest
} from "../requests/categories";

const state = {
  transactions: [],
  offers: [],
  offerForm: {
    beginning_at: null,
    ending_at: null,
    image_url: null,
    gallery_images: null,
    gallery_urls: null,
    terms: null,
    is_active: null,
    percent: null,
    type: null,
    category_id: null,
    title: null,
    image: null,
  },
  categories: [],
  lastCreatedOffer: null,
  balance: null,
  recentTransactions: [],
  lastConfirmedTransaction: null
}

const getters = {}

const mutations = {
  setTransactions: (state, transactions) => {
    state.transactions = transactions
  },
  setLastCreatedOffer: (state, offer) => {
    state.lastCreatedOffer = offer
  },
  setOfferFormField: (state, {field, value}) => {
    if (_.has(state.offerForm, field)) {
      state.offerForm[field] = value;
    }
  },
  setOffer: (state, payload) => {
    Object.keys(payload).forEach((field) => {
        if (_.has(state.offerForm, field)) {
            state.offerForm[field] = payload[field];
        }
    })
  },
  setOffers: (state, offers) => {
    state.offers = offers
  },
  setBalance: (state, payload) => {
    state.balance = payload
  },
  setRecentTransactions: (state, payload) => {
    state.recentTransactions = payload
  },
  setLastConfirmedTransaction: (state, payload) => {
    state.lastConfirmedTransaction = payload
  },
  removeGalleryUrl: (state, payload) => {
    if (!state.offerForm.gallery_images) return;
    state.offerForm.gallery_images = state.offerForm.gallery_images.filter((url) => url instanceof File ? url.name !== payload : url !== payload)
  },
  clearOfferFields: (state) => {
    Object.keys(state.offerForm).forEach((field) => {
        state.offerForm[field] = null;
    })
  },
  setCategories: (state, categories) => {state.categories = categories},
}

const actions = {
  getCustomerTransactions: async ({commit, dispatch}, params) => {
    const transactions = await wrappedPartnerTransactionsRequest(params)
    commit('setTransactions', transactions)
  },
  getCustomerTransaction: async ({commit, dispatch, state}, transactionId) => {
    let transaction = state.transactions && state.transactions.success && state.transactions.data.find(t => t.id == transactionId) || null;

    if (!transaction) {
      transaction = await wrappedPartnerTransactionRequest(transactionId)
      transaction = transaction.success ? transaction.data : null
    }

    return transaction;
  },
  approveTransaction: async ({commit}, transactionId) => {
    const { data, success } = await wrappedPartnerApproveTransactionRequest(transactionId);

    return success ? data : null;
  },
  unApproveTransaction: async ({commit}, transactionId) => {
    const { data, success } = await wrappedPartnerUnApproveTransactionRequest(transactionId);

    return success ? data : null;
  },
  createOffer: async ({commit, dispatch}, params) => {
    commit('setLastCreatedOffer', null)
    const form = new FormData();
    for (const [field, value] of Object.entries(params)) {
        if (Array.isArray(value)) {
            value.forEach((item) => {
                form.append(`${field}[]`, item);
            })
        } else if (value !== null) {
            form.append(field, value);
        }
    }
    const offer = await wrappedPartnerCreateOfferRequest(form)
    commit('setLastCreatedOffer', offer)
  },
  getOffer: async ({commit}, id) => {
    const { data } = await wrappedPartnerOfferRequest(id);
    commit('setOffer', data);
  },
  updateOffer: async ({commit, dispatch}, { id, form }) => {
    commit('setLastCreatedOffer', null)
    const payload = new FormData();
    for (const [field, value] of Object.entries(form)) {
        if (Array.isArray(value)) {
            value.forEach((item) => {
                payload.append(`${field}[]`, item);
            })
        } else if (value !== null) {
            payload.append(field, value);
        }
    }
    payload.append('_method', 'PUT')
    const offer = await wrappedPartnerUpdateOfferRequest({id, payload})
    commit('setLastCreatedOffer', offer)
  },
  getOffers: async ({commit, dispatch}, params) => {
    const offers = await wrappedPartnerOffersRequest(params)
    commit('setOffers', offers)
  },
  getCategories: async ({ commit }) => {
    const { data } = await wrappedCategoriesRequest();
    commit('setCategories', data);
  },
  getBalance: async ({commit, dispatch}, params) => {
    const balance = await wrappedPartnerBalanceRequest(params)
    commit('setBalance', balance)
  },
  getRecentTransactions: async ({commit, dispatch}, params) => {
    params = !params ? {} : params
    params = Object.assign(params, {
      recent: true
    });
    const transactions = await wrappedPartnerTransactionsRequest(params)
    commit('setRecentTransactions', transactions)
  },
  confirmTransaction: async ({commit, dispatch}, transactionId) => {
    commit('setLastConfirmedTransaction', null)
    const lastConfirmedTransaction = await wrappedPartnerConfirmTransactionRequest(transactionId)
    commit('setLastConfirmedTransaction', lastConfirmedTransaction)
  },
  confirmProvisionServiceTransaction: async ({commit, dispatch}, payload) => {
    const r = await wrappedPartnerConfirmProvisionServiceTransactionRequest(payload)
    return r && r.success ? r.data : null;
  },
  clearOfferFields: ({commit}) => commit('clearOfferFields'),
  removeGalleryUrl: ({commit}, url) => commit('removeGalleryUrl', url),
  setOfferFormField: ({commit}, payload) => commit('setOfferFormField', payload),
}


export default {
  namespaced: true,
  state, getters, mutations, actions
}

