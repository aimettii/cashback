import {
    wrappedAdminCustomersRequest, wrappedAdminPartnersRequest,
    wrappedAdminTransactionsRequest,
    wrappedAdminTransactionRequest,
    wrappedAdminApproveTransactionRequest,
    wrappedAdminUnApproveTransactionRequest,
    wrappedAdminUsersRequest,
    wrappedAdminPartnerUpdateBalanceRequest
} from "../requests/admin";
import {
    wrappedCategoryCreateRequest,
    wrappedCategoriesRequest,
    wrappedCategoryUpdateRequest,
    wrappedCategoryDeleteRequest
} from "../requests/categories";

const state = {
    users: [],
    transactions: [],
    partners: [],
    customers: [],
    categories: [],
}

const mutations = {
    setUsers: (state, users) => {state.users = users},
    setTransactions: (state, transactions) => {state.transactions = transactions},
    setCustomers: (state, customers) => {state.customers = customers},
    setPartners: (state, partners) => {state.partners = partners},
    setCategories: (state, categories) => {state.categories = categories},
}

const actions = {
    getUsers: async  ({ commit, dispatch }, params) => {
        const users = await wrappedAdminUsersRequest(params)
        commit('setUsers', users)
    },
    getCustomerTransactions: async  ({ commit, dispatch }, params) => {
        const transactions = await wrappedAdminTransactionsRequest(params)
        commit('setTransactions', transactions)
    },
    getCustomerTransaction: async ({commit, dispatch, state}, transactionId) => {
        const { data, success } = await wrappedAdminTransactionRequest(transactionId);

        return success ? data : null;
    },
    approveTransaction: async ({commit}, transactionId) => {
        const { data, success } = await wrappedAdminApproveTransactionRequest(transactionId);

        return success ? data : null;
    },
    unApproveTransaction: async ({commit}, transactionId) => {
        const { data, success } = await wrappedAdminUnApproveTransactionRequest(transactionId);

        return success ? data : null;
    },
    getCustomers: async  ({ commit, dispatch }, params) => {
        const customers = await wrappedAdminCustomersRequest(params)
        commit('setCustomers', customers)
    },
    getPartners: async  ({ commit, dispatch }, params) => {
        const customers = await wrappedAdminPartnersRequest(params)
        commit('setPartners', customers)
    },
    getCategories: async ({ commit }) => {
        const { data } = await wrappedCategoriesRequest();
        commit('setCategories', data);
    },
    createCategory: async ({}, params) => {
        return await wrappedCategoryCreateRequest(params);
    },
    updateCategory: async ({}, { id, params }) => {
        return await wrappedCategoryUpdateRequest(id, params);
    },
    deleteCategory: async ({ commit }, id) => {
        return await wrappedCategoryDeleteRequest(id);
    },
    updateBalancePartnerRequest: ({ commit }, params) => {
      return wrappedAdminPartnerUpdateBalanceRequest(params);
    }
}

const getters = {
    isUniqueCategoryTitle: state => title => !_.some(state.categories, {'title': title}),
    isUniqueCategoryTitleExceptId: state => (id, title) => {
        const restItems = state.categories.filter((item) => item.id !== id)
        return !_.some(restItems, {'title': title});
    }
}

export default {
    namespaced: true,
    state, getters, mutations, actions
}

