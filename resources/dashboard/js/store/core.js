import Vue from "vue";
import Vuex from "vuex";

import admin from "./admin";
import customer from "./customer";
import partner from "./partner";

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        admin,
        customer,
        partner,
    },
    plugins: [],
    state: {
        footer: true
    },
    getters: {
        // Store in modules
    },
    mutations: {
    },
    actions: {
        // Store in modules
    },
});

export default store;

