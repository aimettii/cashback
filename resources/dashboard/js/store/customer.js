import {
  wrappedCustomerAccountRequest,
  wrappedCustomerFavoriteOfferRequest,
  wrappedCustomerGenerateYodleeTokenRequest,
  wrappedCustomerLinkProviderRequest,
  wrappedCustomerOffersRequest,
  wrappedCustomerTransactionsRequest,
  wrappedCustomerUpdateAccountRequest,
  wrappedCustomerPartnerRequest,
  wrappedCustomerBalanceRequest,
  CustomerGetBankAccountsRequestRequest,
  wrappedCustomerDeleteLinkedBankAccountRequest,
} from '../requests/customer';


const state = {
  account: null,
  transactions: [],
  offers: [],
  newOffers: [],
  yodleeToken: null,
  linkProvider: null,
  favoriteOffer: null,
  partners: [],
  balance: null,
  linkedBankAccounts: null
}

const getters = {}

const mutations = {
  setTransactions: (state, transactions) => {
    state.transactions = transactions
  },
  setAccount: (state, account) => {
    state.account = account
  },
  setAccountName: (state, name) => {
    state.account.name = name
  },
  setAccountEmail: (state, email) => {
    state.account.email = email
  },
  setOffers: (state, offers) => {
    state.offers = offers
  },
  setNewOffers: (state, newOffers) => {
    state.newOffers = newOffers
  },
  setYodleeToken: (state, payload) => {
    state.yodleeToken = payload
  },
  setLinkProvider: (state, payload) => {
    state.linkProvider = payload
  },
  setFavoriteOffer: (state, payload) => {
    state.favoriteOffer = payload
  },
  setPartners: (state, payload) => {
    state.partners = payload
  },
  setBalance: (state, payload) => {
    state.balance = payload
  },
  setLinkedBankAccounts: (state, payload) => {
    state.linkedBankAccounts = payload
  },
  setLinkedBankAccountDeletingState: (state, accountId) => {
    const index = state.linkedBankAccounts.data.findIndex(a => a.id === accountId);
    state.linkedBankAccounts.data[index].deleting = !state.linkedBankAccounts.data[index].deleting;
    state.linkedBankAccounts = { ...state.linkedBankAccounts, data: state.linkedBankAccounts.data}
  },
}

const actions = {
  getCustomerTransactions: async ({commit, dispatch}, params) => {
    const transactions = await wrappedCustomerTransactionsRequest(params)
    commit('setTransactions', transactions)
  },
  getAccountData: async ({commit, dispatch}, params) => {
    const account = await wrappedCustomerAccountRequest(params)
    commit('setAccount', account)
  },
  updateAccountData: async ({commit, dispatch}, params) => {
    await wrappedCustomerUpdateAccountRequest(params)
  },
  getOffers: async ({commit, dispatch}, params) => {
    const offers = await wrappedCustomerOffersRequest(params)
    commit('setOffers', offers)
  },
  getNewOffers: async ({commit, dispatch}, params) => {
    params = !params ? {} : params;
    const newOffers = await wrappedCustomerOffersRequest(
      Object.assign(params, {
        new: true
      })
    );
    commit('setNewOffers', newOffers)
  },
  generateYodleeToken: async ({commit, dispatch}, params) => {
    const token = await wrappedCustomerGenerateYodleeTokenRequest(params)
    commit('setYodleeToken', token)
  },
  linkProvider: async ({commit, dispatch}, params) => {
    const linkProvider = await wrappedCustomerLinkProviderRequest(params)
    commit('setLinkProvider', linkProvider && linkProvider.success || false)
  },
  getLinkedBankAccounts: async ({commit, dispatch}) => {
    const accounts = await CustomerGetBankAccountsRequestRequest()
    commit('setLinkedBankAccounts', accounts)
  },
  deleteLinkedBankAccount: async ({state, commit, dispatch}, accountId) => {
    const accounts = state.linkedBankAccounts.data.slice(0);
    commit('setLinkedBankAccountDeletingState', accountId);
    const deletedAccount = await wrappedCustomerDeleteLinkedBankAccountRequest(accountId);
    commit('setLinkedBankAccountDeletingState', accountId);
    
    if (deletedAccount) {
      if (state.linkedBankAccounts && state.linkedBankAccounts.data) {
        const index = accounts.findIndex(a => a.id === accountId)
        
        if (index > -1) {
          accounts.splice(index, 1)
        }
      }
    }
  
    state.linkedBankAccounts.data = accounts;
    
    commit('setLinkedBankAccounts', state.linkedBankAccounts)
  },
  favoriteOffer: async ({commit, dispatch}, offerId) => {
    const favoriteOffer = await wrappedCustomerFavoriteOfferRequest(offerId)
    commit('setFavoriteOffer', favoriteOffer && favoriteOffer.success || false)
  },
  getPartners: async ({commit, dispatch}, offerId) => {
    const partners = await wrappedCustomerPartnerRequest(offerId)
    commit('setPartners', partners)
  },
  getBalance: async ({commit, dispatch}, offerId) => {
    const balance = await wrappedCustomerBalanceRequest(offerId)
    commit('setBalance', balance)
  },
}


export default {
  namespaced: true,
  state, getters, mutations, actions
}

