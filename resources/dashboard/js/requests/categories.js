import { wrapRequest } from "../helpers";
import axios from "axios";

const CategoriesRequest = () => axios.get('/api/categories');
const CategoryCreateRequest = (params) => axios.post('/api/admin/categories', params);
const CategoryUpdateRequest = (id, params) => axios.put(`/api/admin/categories/${id}`, params);
const CategoryDeleteRequest = (id) => axios.delete(`/api/admin/categories/${id}`);

export const wrappedCategoriesRequest = wrapRequest(CategoriesRequest);
export const wrappedCategoryCreateRequest = wrapRequest(CategoryCreateRequest);
export const wrappedCategoryUpdateRequest = wrapRequest(CategoryUpdateRequest);
export const wrappedCategoryDeleteRequest = wrapRequest(CategoryDeleteRequest);
