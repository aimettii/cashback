import { wrapRequest } from "../helpers";
import axios from "axios";

const AdminUsersRequest = (params) => axios.get('/api/admin/users', {params});
const AdminTransactionsRequest = (params) => axios.get('/api/admin/transactions', {params});
const AdminTransactionRequest = (transactionId) => axios.get(`/api/admin/transactions/${transactionId}`);
const AdminApproveTransactionRequest = (transactionId) => axios.put(`/api/admin/transactions/${transactionId}/approve`);
const AdminUnApproveTransactionRequest = (transactionId) => axios.put(`/api/admin/transactions/${transactionId}/un-approve`);
const AdminCustomersRequest = (params) => axios.get('/api/admin/customers', {params});
const AdminPartnersRequest = (params) => axios.get('/api/admin/partners', {params});
const AdminPartnerUpdateBalanceRequest = (params) => axios.put('/api/admin/partners/' + params.id + '/update-balance', params);

export const wrappedAdminUsersRequest = wrapRequest(AdminUsersRequest);
export const wrappedAdminTransactionsRequest = wrapRequest(AdminTransactionsRequest);
export const wrappedAdminTransactionRequest = wrapRequest(AdminTransactionRequest);
export const wrappedAdminApproveTransactionRequest = wrapRequest(AdminApproveTransactionRequest);
export const wrappedAdminUnApproveTransactionRequest = wrapRequest(AdminUnApproveTransactionRequest);
export const wrappedAdminCustomersRequest = wrapRequest(AdminCustomersRequest);
export const wrappedAdminPartnersRequest = wrapRequest(AdminPartnersRequest);
export const wrappedAdminPartnerUpdateBalanceRequest = wrapRequest(AdminPartnerUpdateBalanceRequest);
