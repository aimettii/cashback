// import '@fortawesome/fontawesome-free/css/all.css' // TODO fix
// import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader

import Vuetify from 'vuetify/lib'

const opts = {
    theme: {
        themes: {
            light: {
                primary: '#C29C63',
                // success: '#18BD5D',
                // warning: '#FF7100',
                // secondary: '#E5F6F9',
                // background: '#EDF2F9;',
                // danger: '#EF0000',
                // error: '#EF0000',

           },
        },
    },
    icons: {
        iconfont: 'mdi', // default - only for display purposes
    },
};

export default new Vuetify(opts)
