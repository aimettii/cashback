@component('mail::message')
Hello!

You need to approve the transaction!

@component('mail::table')
    | Laravel       | Table         |
    | ------------- |:-------------:|
    | TYPE          | {{ $transaction->type }}
    | VALUE         | ${{ $transaction->value }}
    | PARTNER       | {{ $transaction->partner->user->name }}
    | CUSTOMER      | {{ $transaction->customer->user->name }}
@endcomponent

@component('mail::button', [
    'url' => env('APP_URL') . '/dashboard/transactions/' . $transaction->id,
    'color' => 'green'
    ])
    Approved
@endcomponent

{{ config('app.name') }}
@endcomponent
