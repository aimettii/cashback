@component('mail::message')
    Hello!

    Our system has spotted a new transaction for your account

    @component('mail::table')
        | Laravel       | Table         |
        | ------------- |:-------------:|
        | TYPE          | {{ $transaction->type }}
        | VALUE         | ${{ $transaction->value }}
        | CUSTOMER       | {{ $transaction->customer->user->name }}
    @endcomponent

    @if($transaction->type === 'SERVICE')
    Please fill in "service completed" date in your dashboard
    @endif

    {{ config('app.name') }}
@endcomponent