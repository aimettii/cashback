@component('mail::message')
    Hello!

    Our system has spotted a new transaction for your account

    @component('mail::table')
        | Laravel       | Table         |
        | ------------- |:-------------:|
        | TYPE          | {{ $transaction->type }}
        | VALUE         | ${{ $transaction->value }}
        | PARTNER       | {{ $transaction->partner->user->name }}
    @endcomponent

    {{ config('app.name') }}
@endcomponent