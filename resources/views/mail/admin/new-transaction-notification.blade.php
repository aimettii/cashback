@component('mail::message')
    Hello!

    Our system has spotted a new transaction

    @component('mail::table')
        | Laravel       | Table         |
        | ------------- |:-------------:|
        | TYPE          | {{ $transaction->type }}
        | VALUE         | ${{ $transaction->value }}
        | PARTNER       | {{ $transaction->partner->user->name }}
        | CUSTOMER      | {{ $transaction->customer->user->name }}
    @endcomponent

    {{ config('app.name') }}
@endcomponent