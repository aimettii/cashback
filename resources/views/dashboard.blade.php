@extends('layouts.dashboard')

@section('content')
    <v-app>
        <v-app-bar
                app
                class="dashboard-navbar"
                height="60"
        >
            <div>
                <logo/>
            </div>
            <v-spacer></v-spacer>
            <v-toolbar-title class="font-weight-black headline">
            </v-toolbar-title>
            <v-spacer></v-spacer>
            <v-menu offset-y>
                <template v-slot:activator="{ on, attrs }">
                    <v-avatar v-bind="attrs"
                              v-on="on"
                              color="primary">
                        <v-icon dark>mdi-account-circle</v-icon>
                    </v-avatar>
                </template>
                <v-list>
                    <v-list-item
                            href="/logout"
                            v-if="$root.authUser"
                    >
                        <v-list-item-title>Logout</v-list-item-title>
                    </v-list-item>
                </v-list>
            </v-menu>

        </v-app-bar>
        <v-main>
                <div class="w-100 fill-height d-flex flex-nowrap">
                    @if($type == 'customer')
                        <customer-drawer-menu class=""></customer-drawer-menu>
                    @endif
                    @if($type == 'admin')
                        <admin-drawer-menu></admin-drawer-menu>
                    @endif
                    @if($type == 'partner')
                        <partner-drawer-menu></partner-drawer-menu>
                    @endif
                    <v-container class="base-container" fluid :style="{baseContainerWidth}">
                        <transition name="fade" mode="out-in" duration="300">
                            <router-view></router-view>
                        </transition>
                    </v-container>
                </div>
        </v-main>
        <footer-component />
    </v-app>
@endsection
