<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/sanctum/token', 'Api\AuthController@token');

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/account', 'Api\AuthController@getAccount');

    Route::middleware(['can:access-admin'])->group(function () {
        Route::get('/admin/user', 'Api\Admin\UserController@index');
        Route::resource('/admin/transactions', 'Api\Admin\CustomerTransactionController');
        Route::put('/admin/transactions/{id}/approve', 'Api\Admin\CustomerTransactionController@approve')
            ->where('id', '[0-9]+')
            ->name('admin.transactions.approve');
        Route::put('/admin/transactions/{id}/un-approve', 'Api\Admin\CustomerTransactionController@unApprove')
            ->where('id', '[0-9]+')
            ->name('admin.transactions.un-approve');
        Route::resource('/admin/customers', 'Api\Admin\CustomerController');
        Route::resource('/admin/partners', 'Api\Admin\PartnerController');

        Route::resource('/admin/categories', 'Api\Admin\CategoryController')->except('index');

        Route::put('/admin/partners/{id}/update-balance', 'Api\Admin\PartnerController@updateBalance');
    });

    Route::middleware(['can:access-partner'])->group(function () {
        Route::resource('/partner/transactions', 'Api\Partner\CustomerTransactionController');
        Route::post('/partner/update-profile', 'Api\Partner\ProfileController');
        Route::resource('/partner/offers', 'Api\Partner\OfferController')
            ->middleware('optimizeImages');
        Route::get('/partner/balance', 'Api\Partner\BalanceController@index');
        Route::put('/partner/transactions/{id}/confirm', 'Api\Partner\CustomerTransactionController@confirm')
            ->where('id', '[0-9]+');
        Route::put('/partner/transactions/{id}/approve', 'Api\Partner\CustomerTransactionController@approve')
            ->where('id', '[0-9]+')
            ->name('partner.transactions.approve');
        Route::put('/partner/transactions/{id}/un-approve', 'Api\Partner\CustomerTransactionController@unApprove')
            ->where('id', '[0-9]+')
            ->name('partner.transactions.un-approve');
        Route::put('/partner/transactions/{id}/confirm-service-provision', 'Api\Partner\CustomerTransactionController@confirmServiceProvision');
        Route::get('/partner/related-reviews', 'Api\Partner\ReviewsController@index');
        Route::put('/partner/related-reviews/{id}/accept', 'Api\Partner\ReviewsController@acceptReview');
    });

    Route::middleware(['can:access-admin-partner'])->group(function () {
        Route::get('/categories', 'Api\Admin\CategoryController@index');
    });

    Route::middleware(['can:access-customer'])->group(function () {
        Route::resource('/customer/transactions', 'Api\Customer\CustomerTransactionController');
        Route::get('/customer/account', 'Api\Customer\AccountController@index');
        Route::put('/customer/account', 'Api\Customer\AccountController@update');
        Route::get('/customer/offers', 'Api\Customer\OfferController@index');
        Route::post('/customer/offers/{offer_id}/favorite', 'Api\Customer\OfferController@togglefavoriteOffer');
        Route::post('/customer/yodlee/token', 'Api\Customer\YodleeController@token');
        Route::post('/customer/yodlee/link-provider', 'Api\Customer\YodleeController@linkProvider');
        Route::get('/customer/yodlee/get-accounts', 'Api\Customer\YodleeController@getLinkedBankAccounts');
        Route::delete('/customer/yodlee/accounts/{id}', 'Api\Customer\YodleeController@deleteLinkedBankAccounts');
        Route::get('/customer/partners', 'Api\Customer\PartnerController@index');
        Route::get('/customer/balance', 'Api\Customer\BalanceController@index');
        Route::get('/customer/related-offers', 'Api\Customer\ReviewsController@listOffersForReview');
        Route::post('/customer/related-offers/{id}/review', 'Api\Customer\ReviewsController@store');
    });
});

// Front
Route::prefix('offers')
    ->group(function () {
        Route::post('/', 'Api\OfferController@index');
        Route::get('categories', 'Api\OfferController@getCategories');
        Route::get('favorites', 'Api\OfferController@getFavorites');
        Route::get('/{id}/toggle-favorite', 'Api\OfferController@toggleFavorite')
            ->where(['id' => '[0-9]+']);
        Route::get('{slug}', 'Api\OfferController@show');
        Route::get('/{id}/reviews', 'Api\ReviewsController@index');
        Route::put('/{id}/share-add', 'Api\OfferController@addShare');
    });
