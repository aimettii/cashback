<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('exception', function() {
   throw new Exception('exception');
});

Route::get('/{reg?}', function () {
    return view('front.app')->with([
        'type' => 'front'
    ]);
})
    ->where('reg', '^(?!dashboard|storage|logout)(.*)')
    ->name('front');

Route::get('logout', 'Auth\LoginController@logout');
Route::post('login', 'Auth\LoginController@login');

Route::prefix('password')
    ->group(function () {
        Route::post('email', 'Auth\ForgotPasswordController@sendResetLinkEmail')
            ->name('password.email');

        Route::post('reset', 'Auth\ResetPasswordController@reset')
            ->name('password.update');

        Route::get('reset/{token}', 'Auth\ResetPasswordController@showResetForm')
            ->name('password.reset');
    });

Route::prefix('register')
    ->group(function () {
        Route::post('partner', 'Auth\PartnerRegisterController@register');
        Route::post('customer', 'Auth\CustomerRegisterController@register');
    });

Route::get('dashboard/{path?}', 'SpaController@serve')->where('path', '.*')
    ->middleware(['auth']); //->middleware('verified');
Route::any('yodlee-webhook', 'YodleeController@webhook')->name('yodlee-webhook');
