const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.webpackConfig({
  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'resources/js'),
      '@frontendStyles': path.resolve(__dirname, 'resources/sass'),
    }
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        default: false,
        vendors: {
          chunks: 'initial',
          name: 'vendor',
          filename: 'js/dashboard/[name].js'
        }
      }
    }
  }
})
.js('resources/dashboard/js/app.js', 'public/js/dashboard')
.sass('resources/dashboard/sass/app.scss', 'public/css/dashboard')
.mergeManifest();
