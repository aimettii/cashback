<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Partner;
use App\User;
use Faker\Generator as Faker;

$factory->define(Partner::class, function (Faker $faker) {
    $faker->addProvider(new \Bezhanov\Faker\Provider\Avatar($faker));

    return [
        'user_id' => User::inRandomOrder()->first()->id,
        'company_name' => $faker->company,
        'logo' => $faker->avatar,
        'merchant_name' => null,
        'site' => $faker->domainName,
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
    ];
});
