<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Customer;
use App\Models\CustomerTransaction;
use App\Models\Partner;
use Faker\Generator as Faker;

$factory->define(CustomerTransaction::class, function (Faker $faker) {
    return [
        'customer_id' => Customer::inRandomOrder()->first()->id,
        'partner_id' => Partner::inRandomOrder()->first()->id,
        'created_at' => $faker->dateTimeThisMonth,
        'value' => $faker->randomFloat(2, 1, 500)
    ];
});
