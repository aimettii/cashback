<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Balance;
use App\Models\Customer;
use App\Models\Offer;
use App\Models\Partner;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});

$factory->state(App\User::class, 'customer', [
    'type' => User::TYPE_CUSTOMER,
]);
$factory->state(App\User::class, 'admin', [
    'type' => User::TYPE_ADMIN,
]);
$factory->state(App\User::class, 'partner', [
    'type' => User::TYPE_PARTNER,
]);

$factory->afterCreatingState(User::class, 'customer', function (User $user, \Faker\Generator $faker) {
    // 5 pre-registered test accounts on yodlee beginning at sbMem5f3pc24c1a76d(1), ending at sbMem5f3pc24c1a76d(5)
    factory(Customer::class)->create(['user_id' => $user->id, 'yodlee_login_name' => 'sbMem5f3pc24c1a76d' . (Customer::count() + 1)]);
    factory(Balance::class)->create(['user_id' => $user->id, 'value' => 0]);
});

$factory->afterCreatingState(User::class, 'partner', function (User $user, \Faker\Generator $faker) {
    $partner = factory(Partner::class)->create(['user_id' => $user->id]);
    factory(Offer::class, 10)->create(['partner_id' => $partner->id]);
    factory(Balance::class)->create(['user_id' => $user->id, 'value' => $faker->randomFloat(2, 1, 1000)]);
});
