<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Offer;
use App\Models\Partner;
use App\Models\Category;
use Faker\Generator as Faker;

$factory->define(Offer::class, function (Faker $faker) {
    $beginningAt = $faker->dateTimeBetween('now', '+10 days');

    $galleryImages = array_map(function () use ($faker) {
        return getFakeImageUrl(500, false);
    }, array_fill(0, rand(5, 8), ''));

    return [
        'title' => ucfirst($faker->company),
        'subtitle' => $faker->sentence($nbWords = 3, $variableNbWords = true),
        'beginning_at' => $beginningAt,
        'ending_at' => $faker->dateTimeBetween($beginningAt, '+30 days'),
        'partner_id' => Partner::inRandomOrder()->first()->id,
        'category_id' => Category::inRandomOrder()->first()->id,
        'image_url' => getFakeImageUrl(1600, true, 4/3),
        'gallery_images' => json_encode($galleryImages, true),
        'terms' => $faker->sentence($nbWords = 10, $variableNbWords = true),
        'is_active' => $faker->boolean,
        'percent' => $faker->randomFloat(1, 1, 10),
    ];
});
