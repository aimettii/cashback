<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePartnerRegistationRequiredFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partners', function (Blueprint $table) {
            $table->string('site', 100)->nullable()->change();
            $table->string('address', 100)->nullable()->change();
            $table->string('phone', 15)->nullable()->change();
            $table->string('company_name')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('partners', function (Blueprint $table) {
            $table->string('site', 100)->nullable(false)->change();
            $table->string('address', 100)->nullable(false)->change();
            $table->string('phone', 15)->nullable(false)->change();
            $table->string('company_name')->nullable(false)->change();
        });
    }
}
