<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('user_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('yodlee_login_name')->nullable()->unique();
            $table->string('yodlee_access_token')->nullable();
            $table->dateTime('yodlee_token_issued_at')->nullable();
            $table->integer('yodlee_token_expires_in')->nullable();
            $table->string('yodlee_linked_bank')->nullable();
            $table->unsignedInteger('yodlee_linked_provider_id')->nullable();
            $table->unsignedInteger('yodlee_linked_provider_account_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
