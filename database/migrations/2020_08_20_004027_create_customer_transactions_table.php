<?php

use App\Models\CustomerTransaction;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('partner_id');
            $table->decimal('value', 8, 2);
            $table->dateTime('released_at')->nullable();
            $table->dateTime('service_completed_at')->nullable();
            $table->boolean('approved_by_admin')->default(false);
            $table->boolean('approved_by_partner')->default(false);
            $table->enum('type', [CustomerTransaction::TYPE_SERVICE, CustomerTransaction::TYPE_RETAIL])->nullable(); // mb not found active offer for transaction
            $table->enum('status', [CustomerTransaction::STATUS_PENDING, CustomerTransaction::STATUS_COMPLETED])
                ->default(CustomerTransaction::STATUS_PENDING);
            $table->bigInteger('yodlee_unique_id');
            $table->json('data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_transactions');
    }
}
