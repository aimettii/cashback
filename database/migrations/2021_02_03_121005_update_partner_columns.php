<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePartnerColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partners', function (Blueprint $table) {
            $table->string('facebook')->after('phone')->nullable();
            $table->string('instagram')->after('phone')->nullable();
            $table->string('linkedin')->after('phone')->nullable();
            $table->string('description_short')->after('phone')->nullable();
            $table->string('description_large')->after('phone')->nullable();
            $table->string('contact_image')->after('phone')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('partners', function (Blueprint $table) {
            $table->dropColumn('facebook');
            $table->dropColumn('instagram');
            $table->dropColumn('linkedin');
            $table->dropColumn('description_short');
            $table->dropColumn('description_large');
            $table->dropColumn('contact_image');
        });
    }
}
