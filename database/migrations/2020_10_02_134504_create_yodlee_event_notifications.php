<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateYodleeEventNotifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yodlee_event_notifications', function (Blueprint $table) {
            $table->id();
            $table->string('login_name');
            $table->string('from_date');
            $table->string('to_date');
            $table->boolean('handled')->default(false);
            $table->boolean('failed')->default(false);
            $table->json('data_extracted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yodlee_event_notifications');
    }
}
