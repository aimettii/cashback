<?php

use App\Models\Offer;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->id();
            $table->string('slug');
            $table->string('title');
            $table->string('subtitle')->nullable();
            $table->date('beginning_at');
            $table->date('ending_at');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('partner_id');
            $table->string('image_url')->nullable();
            $table->json('gallery_images');
            $table->text('terms');
            $table->boolean('is_active')->default(false);
            $table->decimal('percent', 5, 2);
            $table->enum('type', [Offer::TYPE_SERVICE, Offer::TYPE_RETAIL]);
            $table->unsignedInteger('views')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
