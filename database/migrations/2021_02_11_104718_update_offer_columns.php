<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOfferColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->string('slug')->nullable()->change();
        });
        $offers = \App\Models\Offer::all();

           foreach ($offers as $offer) {
               $offer->update(['slug' => null]);
           }

        Schema::table('offers', function (Blueprint $table) {
            $table->json('gallery_images')->nullable()->change();
            $table->string('slug')->nullable(false)->unique()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->json('gallery_images')->nullable(false)->change();
            $table->json('slug')->unique(false)->change();
        });
    }
}
