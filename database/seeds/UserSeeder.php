<?php

use App\Models\Balance;
use App\Models\Offer;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->states(['admin'])->create(['email' => 'admin@example.com']);
        factory(User::class)->states(['customer'])->create(['email' => 'customer@example.com']);
        $partnerUser = factory(User::class)->create([
            'email' => 'uber@example.com',
            'type' => User::TYPE_PARTNER,
        ]);

        $partner = \App\Models\Partner::create([
            'user_id' => $partnerUser->id,
            'logo' => 'https://c7.hotpng.com/preview/924/678/471/5bbf53e076aa0.jpg',
            'merchant_name' => 'Uber',
            'company_name' => 'Uber',
            'site' => 'https://uber.com',
            'address' => '12 Smith St, Melburn',
            'phone' => '+61 7 1234 1234'
        ]);

        factory(Offer::class)->create(['partner_id' => $partner->id, 'beginning_at' => '2020-10-04T15:59:38.000000Z', 'ending_at' => '2020-10-06T15:59:38.000000Z', 'type' => 'SERVICE']);

        factory(Balance::class)->create(['user_id' => $partnerUser->id, 'value' => 1000]);

        factory(User::class, 4)->states(['customer'])->create();
    }
}
