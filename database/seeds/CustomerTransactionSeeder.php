<?php

use App\Models\Balance;
use App\Models\CustomerTransaction;
use App\Models\Partner;
use App\User;
use Illuminate\Database\Seeder;

class CustomerTransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(CustomerTransaction::class, 5000)->create();

//        foreach (User::customers()->get() as $user) {
//            $this->_calculateUserBalance($user);
//        }
    }

    private function _calculateUserBalance($user): void
    {
        $transactions = $user->customer->customerTransactions()->sum('value');
        Balance::create([
           'user_id' =>$user->id,
           'value' => $transactions,
        ]);
    }
}
