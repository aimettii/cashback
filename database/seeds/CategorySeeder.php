<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * @var string[][] $categories
     */
    private $categories;

    /**
     * CategorySeeder constructor.
     */
    public function __construct()
    {
        $this->categories = [
            [
                'title' => 'Aviation',
                'icon' => 'mdi-airplane'
            ],
            [
                'title' => 'Resorts',
                'icon' => 'mdi-beach'
            ],
            [
                'title' => 'Golf',
                'icon' => 'mdi-golf'
            ],
            [
                'title' => 'Retail',
                'icon' => 'mdi-basket'
            ],
            [
                'title' => 'Ground Transport',
                'icon' => 'mdi-car-estate'
            ],
            [
                'title' => 'Wellness',
                'icon' => 'mdi-spa'
            ],
            [
                'title' => 'Events',
                'icon' => 'mdi-calendar-star'
            ],
            [
                'title' => 'Super Yachts',
                'icon' => 'mdi-ferry'
            ],

        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->categories as $category) {
            factory(App\Models\Category::class)->create($category);
        }
    }
}
